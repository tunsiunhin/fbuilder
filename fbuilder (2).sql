-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th9 10, 2018 lúc 11:05 AM
-- Phiên bản máy phục vụ: 5.7.19
-- Phiên bản PHP: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `fbuilder`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `accounts`
--

CREATE TABLE `accounts` (
  `id` int(20) NOT NULL,
  `acc_user` int(20) NOT NULL,
  `acc_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `acc_fbid` bigint(20) NOT NULL,
  `acc_token` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `acc_picture` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `acc_status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `accounts`
--

INSERT INTO `accounts` (`id`, `acc_user`, `acc_name`, `acc_fbid`, `acc_token`, `acc_picture`, `acc_status`) VALUES
(7, 1758, 'Tuấn Luu', 100009735272482, 'EAAAAUaZA8jlABAHvnzlQah4IerUQ7dpZB0UmPl7KZBZAny9rEHiAgX0D4gcTx3XvVw6ZCU9D3fQr4fOu8ZAZArFvIH4HZAP83tDqmpecDKGzggXbCUvQstlQSpdDZAf57W4dh4cwiHA1Wl4kbDnQEe1GiZBqjZA6YDxuBcZD', 'https://graph.facebook.com/100009735272482/picture', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `logs`
--

CREATE TABLE `logs` (
  `log_id` int(20) NOT NULL,
  `mess_error` text COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `page_id` int(20) NOT NULL,
  `account_id` int(11) NOT NULL,
  `access_token` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(20) NOT NULL,
  `page_fbid` bigint(30) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `fan_count` int(11) NOT NULL,
  `error_token` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pages`
--

INSERT INTO `pages` (`page_id`, `account_id`, `access_token`, `user_id`, `page_fbid`, `name`, `picture`, `fan_count`, `error_token`) VALUES
(13, 7, 'EAAAAUaZA8jlABAHjPxC0Ml5ZB9b8k0vWJzfzdZBvemMqp2N0KijRRclMZBJDfdYyFduzXBxyXoj9eM7Xj1Q9BbTsRMZAqJsA4SlNOKioLTgRLE5ZBB4Kmi6eLX5NfHlI9DlmLWRvzZBVlZBTGCKIU7UMq9pZB7IrxnsPeiIiA4U4dBwZDZD', 1758, 405351066548296, 'Art Manga', 'https://graph.facebook.com/405351066548296/picture', 1, 0),
(14, 7, 'EAAAAUaZA8jlABALZCNBUMQ33RGurBdYoWMceGu348fcxYDw7uODgbRE6QibhZB4X7luwuxQ3oXqiLp3UyeZCQm749W0jdJrEqGO6jP8Sozn5rL5xQyCu4odfHmKmT2f2UGv4IZBSCZAQumcZBYNob4DN1c2hZAazBJ0pESliLNMixAZDZD', 1758, 588692961311680, 'Sama Plus', 'https://graph.facebook.com/588692961311680/picture', 62, 0),
(15, 7, 'EAAAAUaZA8jlABAJNMgydz6Kn1oZAd8ZAPUY7XFamypAZAqFVuZABloTTpZCjPkbfIKJ8zAN2PT3rrDJHyMQ16xKdvnBVrxXaz5csHzXrdYwWuJc4rMwuF3NcWRvHZAGDEscKczL1EtlFzXLVVbZCdQ0gCK32zbmq0tITYVerrlv2aQZDZD', 1758, 1106712226019610, 'Nguyen Vu Team', 'https://graph.facebook.com/1106712226019610/picture', 14554, 0),
(16, 7, 'EAAAAUaZA8jlABAP5SqcoUqKfbdqlU8uSCGzeTfMUFnKdZBcixt0m5op04VUCPeckFKD8Xb7NS6kG4dE60aN5fy5ZAeeOdhzWUoPIQd6mZAbliHwoZA5nChs2cSG1SywwTriiFcsMtFvrzYMkzw0R5dYWkzWS9VTF53ZCl6Cz4qwwZDZD', 1758, 489245067914337, 'Truyện Tranh Comic', 'https://graph.facebook.com/489245067914337/picture', 77, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `repost`
--

CREATE TABLE `repost` (
  `repost_id` int(20) NOT NULL,
  `page_id` int(30) NOT NULL,
  `setting` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `result` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `result_repost`
--

CREATE TABLE `result_repost` (
  `rrepost` int(20) NOT NULL,
  `page_id` int(20) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `time_posted` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `schedule`
--

CREATE TABLE `schedule` (
  `schedule_id` int(20) NOT NULL,
  `page_id` int(20) NOT NULL,
  `setting` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `result` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `source_repost`
--

CREATE TABLE `source_repost` (
  `srepost_id` int(20) NOT NULL,
  `repost_id` int(20) NOT NULL,
  `page_fbid` int(30) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `fan_count` int(20) NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `source_schedule`
--

CREATE TABLE `source_schedule` (
  `sschedule_id` int(20) NOT NULL,
  `schedule_id` int(20) NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `post_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `time_posted` int(20) NOT NULL,
  `link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `time_create` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(20) NOT NULL,
  `exprice` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user_info`
--

INSERT INTO `user_info` (`user_id`, `exprice`) VALUES
(1758, 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Chỉ mục cho bảng `repost`
--
ALTER TABLE `repost`
  ADD PRIMARY KEY (`repost_id`);

--
-- Chỉ mục cho bảng `result_repost`
--
ALTER TABLE `result_repost`
  ADD PRIMARY KEY (`rrepost`);

--
-- Chỉ mục cho bảng `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Chỉ mục cho bảng `source_repost`
--
ALTER TABLE `source_repost`
  ADD PRIMARY KEY (`srepost_id`);

--
-- Chỉ mục cho bảng `source_schedule`
--
ALTER TABLE `source_schedule`
  ADD PRIMARY KEY (`sschedule_id`);

--
-- Chỉ mục cho bảng `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `repost`
--
ALTER TABLE `repost`
  MODIFY `repost_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `result_repost`
--
ALTER TABLE `result_repost`
  MODIFY `rrepost` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `schedule`
--
ALTER TABLE `schedule`
  MODIFY `schedule_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `source_repost`
--
ALTER TABLE `source_repost`
  MODIFY `srepost_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `source_schedule`
--
ALTER TABLE `source_schedule`
  MODIFY `sschedule_id` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
