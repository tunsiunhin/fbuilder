// JavaScript Document
var page_id;
function notify(type, message){
    $.notify({
        title: '<strong>Fbuilder : </strong>',
        icon: '',
        message: message
    }, {
        type: type,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutRight'
        },
        placement: {
            from: "bottom",
            align: "left"
        },
        offset: {
            x: 10,
            y: 20
        },
        spacing: 3,
        deflay: 2000,
        timer: 1000,
        z_index: 1031,
    });
}
function request(url,param)
{
		var rq = $.ajax({
			url:url,
			data:param,
			type:'post',
			dataType:"json",
			success: function(res) {
	
					var notiType = 'info';
					if(res.code == 400)
					{
						notiType = 'danger';	
					}
					else if(res.code == 200)
					{
						notiType = 'success';	
					}else if(res.code == 199)
					{
						notiType = 'warning';
					}
		
					
					notify(notiType,res.message);	
			}
		});	
		return rq;	
}

