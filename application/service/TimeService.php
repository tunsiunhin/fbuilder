<?php
namespace Application\Service;

class timeService
{

    public function get_time($delay, $exec_time, $type)
    {
        print_r($delay);
        die;
        $timetoday = strtotime('TODAY');

        $start = $timetoday + $exec_time[0] * 3600;

        $end = $timetoday + $exec_time[1] * 3600;

        if ($end < $start) {

            if (intval(date('H')) < $exec_time[0]) {
                $start -= 86400;
            } else {
                $end += 86400;
            }

        }

        if ($type == 1) {

            $delay = rand($delay[0], $delay[1]) * 60;

            $nexttime = $delay + time();

            $new_day = false;

            if ($nexttime >= $end) {

                $nexttime = $start + 86400;
                $new_day = true;
            }

            return array('nexttime' => $nexttime, 'new_day' => $new_day);
        } else {

            $nexttime = $start + 86400;
            return $nexttime;
        }

    }
}