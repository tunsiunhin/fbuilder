<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $user_id;
	public $user_email;
	public $accounts;
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		session_start();
		$this->check_session();
		
 	}
	
	public function view($view)
	{
		$this->load->view('template/header');
		$this->load->view($view,$this->data);
		$this->load->view('template/footer');
	}
	
	private function check_session()
	{
		if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_email']))
		{
			$this->unauthorized();
		}
		
		$this->user_id    = $_SESSION['user_id'];
		$this->user_email = $_SESSION['user_email'];
		$this->accounts   = isset($_SESSION['account']) ? $_SESSION['account'] : array();
		if(!$this->accounts)
		{
			$info = $this->getInfo($this->user_id);
			$this->accounts = $info ? $info : array();
		
		}
		
		date_default_timezone_set("America/Los_Angeles");

	}
	
	private function unauthorized()
	{

		$verify_url = urlencode(base_url().'authentication');
		redirect('https://auth.fptultimate.com?redirect='.$verify_url);	
		die;		
	}
	public function getInfo($user_id)
	{
		$info = $this->db->select('acc_token,acc_status')->where('acc_user',$user_id)->get('accounts')->result_array();
		if(!$info)
			return array();

		return $info;
	}
	private function unactive()
	{
		/*$user = $this->db->select('status')->where('user_id',$this->user_id)->get('users')->row_array();
		if($user['status'] == 1) {
			$_SESSION['user_status'] = $user['status'];	
			$this->user_status = $user['status'];
			return;
		}
		
		if($this->router->fetch_method() != 'unactive'){
			redirect(base_url().'unactive');
			die;	
		}*/
	}	
}
?>