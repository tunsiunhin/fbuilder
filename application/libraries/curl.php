<?php
class Curl
{
	public function call($url, $param = '', $header_custom = '', $cookie='')
	{
		$header = array('Accept: application/json', 'Content-Type: application/json');
		if($header_custom)
			$header = $header_custom;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
		if($param){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$param);
		}
		if($cookie){
			curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().'/'.$cookie);
			curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().'/'.$cookie);
		}
		$res = curl_exec($ch);
		curl_close($ch);
		return $res;
	}

    public function fb_api($path, $method = 'GET', $data = NULL,$version = 'v2.9/')
    {


        $url = 'https://graph.facebook.com/'.$version.$path;

        $headers = array('Accept: application/json', 'Content-Type: application/json', );
        $ch    = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        switch($method) {
            case 'GET' :
                break;
            case 'POST' :
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'PUT' :
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'DELETE' :
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }
        $response  = curl_exec($ch);

        curl_close($ch);
        return json_decode($response, true);
    }

	public function fb_call($path,$data = NULL)
	{
		$url = 'https://graph.facebook.com/v3.1/'.$path;
		$headers = array('Accept: application/json', 'Content-Type: application/json', );

		$ch    = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		if($data){
			curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		}
		$response  = curl_exec($ch);
		curl_close($ch);
		return json_decode($response, true);

	}
		public function pinterest_call($source_url,$data){
		$header = array(
            'Accept: application/json, text/javascript, */*, q=0.01',
            'Accept-Encoding: utf-8',
            'Accept-Language:vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2',
            'Connection:keep-alive',
            'Host: www.pinterest.com',
            'Referer: https://www.pinterest.com/',
            'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) Chrome/61.0.3163.100 Safari/537.36',
            'X-APP-VERSION: 51031ef',
            'X-Pinterest-AppState: background',
            'X-Requested-With:XMLHttpRequest'
        );
		$url = 'https://www.pinterest.com/resource/SearchResource/get/?'.$source_url.'&'.$data;
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().'/vPVLT9erxi.txt.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR,  getcwd().'/vPVLT9erxi.txt.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36");
        curl_setopt($ch, CURLOPT_REFERER,'https://www.pinterest.com/');
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        $response = curl_exec($ch);
		return $response;
       	return json_decode($response,true);
	}
	public function amz_upload($link,$id,$forder,$temp_file)
	{
		$accessKeyId = 'AKIAJ4M2NJ5RHUDU6NIQ';
		$secretKey = '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';
		$bucket = 'adssuccess';
		$region = 'ap-southeast-1';
		$acl = 'public-read';
		$fileName = $id.'.jpg';
		$fileType = 'image/jpeg';

		$policy = base64_encode(json_encode(array(
			'expiration' => gmdate('Y-m-d\TH:i:s\Z', time() + 86400),
			'conditions' => array(
				array('acl' => $acl),
				array('bucket' => $bucket),
				array('starts-with', '$key', ''),
				array('starts-with', '$Content-Type', '')
			)
		)));

		$signature = hash_hmac('sha1', $policy, $secretKey, true);
		$signature = base64_encode($signature);

		$url = 'https://'.$bucket . '.s3-' . $region . '.amazonaws.com';

		$p = file_get_contents($link);
		file_put_contents($tmp_file,$p);

		$post = array(
			'key' => $forder.'/'.$fileName,
			'AWSAccessKeyId' =>  $accessKeyId,
			'acl' => $acl,
			'policy' =>  $policy,
			'Content-Type' =>  $fileType,
			'signature' => $signature,
			'file' => new CurlFile(realpath($tmp_file), $fileType, $fileName)
		);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);

		$response = curl_exec($ch);

		if($response != '')
		{
			$amz_link = $url.'/'.$forder.'/'.$fileName;
			return array('success'=>1,'url'=>$amz_link);
		}else
		{
			return array('success'=>0,'url'=>0);
		}

	}
	public function fb_push($url,$data){
		$headers = array('Accept: application/json', 'Content-Type: application/json', );
		$ch    = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$response  = curl_exec($ch);
		curl_close($ch);
		return json_decode($response, true);
	}
	public function fb_insights($arr = array(),$token = ''){
				$mh = curl_multi_init();
				$curly = array();
				$headers = array('Accept: application/json', 'Content-Type: application/json', );
				foreach($arr as $k => $ar){
					$url = 'https://graph.facebook.com/v2.10/'.$ar['id'].'/insights/post_impressions_unique,post_consumptions,post_reactions_like_total?fields=values&access_token='.$token;
					$curly[$k] = curl_init();
					curl_setopt($curly[$k], CURLOPT_URL,$url);
					curl_setopt($curly[$k], CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curly[$k], CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($curly[$k], CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($curly[$k], CURLOPT_RETURNTRANSFER, TRUE);
					curl_multi_add_handle($mh, $curly[$k]);

					};
					do{
						//usleep(500000); // delay 0.5s
						usleep(10000);
						curl_multi_exec($mh, $running);
					}while($running > 0);
					foreach($curly as $k =>$dt){
						$r = json_decode(curl_multi_getcontent($dt),true);
						$res[] = $r;
						curl_multi_remove_handle($mh, $dt);
					}
					curl_multi_close($mh);
					return $res;
	}
}
?>