<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('repost_model');
		$this->load->helper('response');
		$this->load->library('curl');
	}

	public function repost(){
		$repost_id = $this->input->post('repost_id');
		$page_id = $this->input->post('page_id');
		$setting = $this->input->post('setting');
		$setting = json_encode($setting);
		if($repost_id == 0)
		{
			$insert = array(
				'page_id' => $page_id,
				'setting' => $setting,
				'status' => 1,
				'result' => 0,
				'user_id' => $this->user_id
			);
			$c = $this->repost_model->addRepost($insert);
			print_r($c);
		}else
		{
			$update = array(
				'setting' => $setting
			);
			$c = $this->repost_model->updateRepost($repost_id, $update);
			if($c)
				print_r($repost_id);

		}
	}

	public function search(){
		$key = $this->input->post('keyword');
		$token = $this->accounts[0]['acc_token'];
		$path = 'search?q='.$key.'&limit=100&type=page&fields=name,picture,fan_count&access_token='.$token;
		$pages = $this->curl->fb_call($path);
		$html = '';
		foreach ($pages['data'] as $row) {
			$html .= '<li data-key="'.$row['id'].'">'
             . '<div class="hidden">'.json_encode($row).'</div>'
             . '<img src="'.$row['picture']['data']['url'].'" />'
             . '<div>'
             . '<label><a target="_blank" href="https://www.facebook.com/'.$row['id'].'">'.$row['name'].'</a></label>'
             . '<span>'.number_format($row['fan_count']).' Likes</span>'
             . '</div></li>';
		}
		echo $html;
	}

	public function saveSourceRepost(){
		$data = $this->input->post('data');
		$repost_id = $this->input->post('repost_id');
		//$data = json_decode($data);
		$insert = array(
			'repost_id' => $repost_id,
			'page_fbid' => $data['id'],
			'name' => $data['name'],
			'fan_count' => $data['fan_count'],
			'picture' => $data['picture']['data']['url']
		);
		$c = $this->repost_model->addSourceRepost($insert);
		echo $c;
	}

	public function removeSourceRepost(){
		$repost_id = $this->input->post('repost_id');
		$page_fbid = $this->input->post('page_fbid');
		$where = array(
			'repost_id' => $repost_id,
			'page_fbid' => $page_fbid
		);
		$c = $this->repost_model->removeSourceRepost($where);
		echo $c;
	}

	public function setStatus(){
		$repost_id = $this->input->post('repost_id');
		$status = $this->input->post('status');

		$c = $this->repost_model->setStatus($repost_id, $status);
	}

	
   	public function change_action()
	{
		$page_id = $this->input->post('page_id');
		$action  = ($this->input->post('action_id') && $this->input->post('action_id') == 1) ? 'repost' : 'schedule';
		if(!$page_id)
		{
			echo response(400,'Error Params');
			exit();
		}
		$info = $this->db->select('status')->where(array('page_id'=>$page_id,'user_id'=>$this->user_id))->get($action)->row_array();
		if(!$info)
		{
			echo response(199,'Vui lòng cài đặt trước khi bật/tắt');
			exit();
		}
		$status = ($info && $info['status'] == 1) ? 0 : 1;
		$check = $this->db->where(array('page_id'=>$page_id,'user_id'=>$this->user_id))->set(array('status'=>$status))->update($action);
		if($check)
		{
			echo response(200,'Thành Công');
		 	exit();
		}else
		{
			echo response(199,'Error');
		 	exit();
		}
	}
    public function save_schedule()
    {
    	$page_id = $this->input->post('page_id');
    	$setting = $this->input->post('setting');
    	if(!$page_id || !$setting)
    	{
    		echo reponse(400,'Error Params');
    		exit();
    	}
    	
    	$data = array(
    		'time_run'=>[0,23],
    		'post_per_day'=>[5,10],
    		'time_delay_post'=>[50,100],
    		'add_tag'=>0,
    		'hashtag'=>''

    	);
    	if(isset($setting['time_run'][0]) && isset($setting['time_run'][1]))
    	{
    		$data['time_run'] = $setting['time_run'];
            if($setting['time_run'][0] > 23)
            {
                $setting['time_run'][0] = 23;
            }
            if($setting['time_run'][1] > 23)
            {
                $setting['time_run'][1] = 23;
            }
    	}
    	if(isset($setting['post_per_day'][0]) && isset($setting['post_per_day'][1]))
    	{
    		$data['post_per_day'] = $setting['post_per_day'];
    	}
    	if(isset($setting['time_delay_post'][0]) && isset($setting['time_delay_post'][1]))
    	{
    		$data['time_delay_post'] = $setting['time_delay_post'];
    	}
    	if(isset($setting['add_tag'][0]))
    	{
    		$data['add_tag'] = $setting['add_tag'][0];
    	}
    	if(isset($setting['add_tag'][0]))
    	{
    		$data['hashtag'] = $setting['hashtag'][0];
    	}
    	$old_setting = $this->db->where('page_id',$page_id)->get('schedule')->row_array();
    	if($old_setting)
    	{
    		$this->db->where('page_id',$page_id)->set(array('setting'=>json_encode($data)))->update('schedule');
    	}else
    	{
    		$this->db->insert('schedule',array('page_id'=>$page_id,'user_id'=>$this->user_id,'setting'=>json_encode($data),'status'=>1,'result'=>0));
    	}
    	echo response(200,'Success');
    	exit();
    }
    public function postSchedule()
    {
        $data = $this->input->post();
        if(!$data || !isset($data['page_id']))
        {
            echo response(400,'Error params');
            exit();
        }
        $schedule = $this->db->where(array('page_id'=>$data['page_id'],'user_id'=>$this->user_id))->get('schedule')->row_array();
        if(!$schedule)
        {
            echo response(400,'Bạn chưa setting cho page');
            exit();
        }
<<<<<<< HEAD
        if(!isset($data))
        {
            exit();
        }
        if(isset($data['source']))
        {
            $type = 'video';
        }else if(isset($data['picture']))
        {
            $type = 'photo';
        }else if(empty($data['source']) && empty($data['picture']))
        {
            $type = 'status';
=======
        $type = 'status';

        if(!empty($data['source']))
        {
            $type = 'video';
        }else if(!empty($data['picture']))
        {
            $type = 'photo';
>>>>>>> 401d9b8779d710933aed08e7025b617a4104c8b2
        }

        $post = array(
            'schedule_id'=>$schedule['schedule_id'],
            'picture' => isset($data['picture']) ? $data['picture'] : '',
            'content' => isset($data['text']) ? $data['text'] : '',
            'link'  => '',
            'status' => 0,
<<<<<<< HEAD
            'type'   => $type ,
            'source' => isset($data['source']) ? $data['source'] : '',
            'time_create' => time()
        );
        print_r($post);
=======
            'source' => isset($data['source']) ? $data['source'] :'',
            'type' => $type,
            'time_create' => time()
        );
        
>>>>>>> 401d9b8779d710933aed08e7025b617a4104c8b2
        $this->db->insert('source_schedule',$post);
        echo response(200,'Lưu Thành Công');
        exit();
    }
    public function removePost()
    {
    	$id = $this->input->post('id');
        $page_id = $this->input->post('page_id');
    	if(!$id || !$page_id)
    	{
    		echo response(400,'Error Params');
       	 	exit();
    	}
    	$setting = $this->db->where(array('user_id'=>$this->user_id,'page_id'=>$page_id))->get('schedule')->row_array();
    	if(!$setting)
    	{
    		echo response(400,'Error Auth');
       	 	exit();
    	}
    	$this->db->where(array('sschedule_id'=>$id,'schedule_id'=>$setting['schedule_id']))->delete('source_schedule');
    	echo response(200,'Xóa Thành Công');
       	exit();
    }
    public function editPost()
    {
    	$id = $this->input->post('id');
    	$content = $this->input->post('content');
        $page_id = $this->input->post('page_id');
    	if(!$id || !$page_id)
    	{
    		echo response(400,'Error Params');
       	 	exit();
    	}
    	$setting = $this->db->where(array('user_id'=>$this->user_id,'page_id'=>$page_id))->get('schedule')->row_array();
    	
    	if(!$setting)
    	{
    		echo response(400,'Error Auth');
       	 	exit();
    	}
    	$data = $this->db->where(array('sschedule_id'=>$id,'schedule_id'=>$setting['schedule_id']))->update('source_schedule',array('content'=>$content));
    	echo response(200,'Sửa Thành Công');
       	exit();
    }
  
}
