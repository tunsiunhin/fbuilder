<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        $this->load->database();
        $this->load->helper('date');
    }

    public function index()
    {
        $user = $this->db->where(array('exprice >' => time(), 'status' => 1))->get('user_info')->row_array();

        if ($user) {
            //them vao
            $schedule = "* * * * * php /home/fbuilder.pro/public_html/index.php cronjob schedule " . $user['user_id'];
            $repost = "* * * * * php /home/fbuilder.pro/public_html/index.php cronjob repost " . $user['user_id'];
            exec("(crontab -l ; echo '" . $schedule . "') | crontab");
            exec("(crontab -l ; echo '" . $repost . "') | crontab");
			$this->db->where('user_id', $user['user_id'])->set('status', 2)->update('user_info');
		
        }

    }

    public function schedule($user_id)
    {
		
        $this->db->select('schedule.schedule_id, schedule.page_id, schedule.setting, schedule.status, schedule.result,
                            schedule.result_day, 
                            pages.access_token, pages.page_fbid');
        $this->db->from('pages');
        $this->db->join('user_info', 'pages.user_id = user_info.user_id');
        $this->db->join('schedule', 'pages.page_id = schedule.page_id');
        $this->db->where('pages.user_id', $user_id);
        $this->db->where('schedule.status', 1);
        $this->db->where('time_run <', time());
        $setting = $this->db->get()->result_array();
		if($test)
			print_r($setting);
        if (!$setting)
		{
            exit();
        }

        foreach ($setting as $data) {

            $setup = json_decode($data['setting'], true);

            // result max day, update time run
            $max_day = rand($setup['post_per_day'][0], $setup['post_per_day'][1]);
            if ($data['result_day'] > $max_day)
            {
                $update_schedule['result_day'] = 0;
                $update_schedule['time_run'] = $this->get_time($setup['time_delay_post'],$setup['time_run'],2);
                $this->db->where('schedule_id', $data['schedule_id'])->set($update_schedule)->update('schedule');
                continue;
            }

            $post = $this->db->where('schedule_id', $data['schedule_id'])
                    ->where('status', 0)
                    ->order_by("sschedule_id", "asc")->limit(1)
                    ->get('source_schedule')->row_array();

            if (!$post){

                continue;
            }

            $get_time = $this->get_time($setup['time_delay_post'], $setup['time_run'],1);

            if (!empty($post['picture'])) {
                $param = array(
                    'access_token' => $data['access_token'],
                    'url' => $post['picture'],
                    'caption' => $post['content'] ."\n". $setup['hashtag'],
                );

                $path = $data['page_fbid'] . '/photos';


            } elseif (!empty($post['link'])){
                $param = array(
                    'access_token' => $data['access_token'],
                    'file_url' 		=> $post['link'],
                    'description' => $post['content'] ."\n". $setup['hashtag'],
                );

                $path =  $post['page_fbid'].'/videos';
            } else {

                $param = array(
                    'access_token' => $data['access_token'],
                    'message' => $post['content'] ."\n". $setup['hashtag'],
                );

                $path = $data['page_fbid'] . '/feed';
            }
            $post_comment = $this->curl->fb_api($path, 'POST', $param);

            // success
            if (isset($post_comment['id']))
            {

                $update['result'] = $data['result'] + 1;
                $update['result_day'] = $data['result_day'] + 1;
                $update['time_run'] = $get_time['nexttime'];
                $this->db->where('schedule_id', $data['schedule_id'])->set($update)->update('schedule');

                // update status source schedule
                $this->db->where('sschedule_id', $post['sschedule_id'])->set(array('status'=>1,'time_posted'=>time(),'post_id'=>$post_comment['id']))->update('source_schedule');

            } else {
				 // error
				$log = array(
					'mess_error' => $post_comment['error']['code'].' - '.$post_comment['error']['message'],
					'action'	=> 'repost',
					'page_id'	=> $data['page_id'],
					'user_id'	=> $user_id,
					'time_log'	=> time()
				);
               
                $this->db->insert('logs', $log);

            }
        }
    }

    public function repost($user_id)
	{
		echo '<pre>';
		$test = $this->input->get('test') ? $this->input->get('test') : false;
		
        $this->db->select('repost.repost_id, repost.page_id, repost.setting, repost.result_day, repost.result, 
                    repost.status, pages.access_token, pages.page_fbid');
        $this->db->from('pages');
        $this->db->join('user_info', 'pages.user_id = user_info.user_id');
        $this->db->join('repost', 'pages.page_id = repost.page_id');
        $this->db->where('pages.user_id', $user_id);
        $this->db->where('repost.status', 1);
        $this->db->where('time_run <', time());
        $setting = $this->db->get()->result_array();
		if($test)
			print_r($setting);
        if (!$setting)
		{
            exit();
        }
        foreach ($setting as $data) {
            $setup = json_decode($data['setting'], true);

            // result max day, update time run
            $max_day = rand($setup['post_day'][0], $setup['post_day'][1]);

            if ($data['result_day'] > $max_day)
            {
                $update_repost['result_day'] = 0;
                $update_repost['time_run'] = $this->get_time($setup['delay_ope'],$setup['execute_time'],2);
                $this->db->where('repost', $data['repost_id'])->set($update_repost)->update('repost');
                continue;
            }

            do {

                $source = $this->db->where('repost_id', $data['repost_id'])
                    ->order_by('rand()')->limit(1)
                    ->get('source_repost')->row_array();
				if($test)
					print_r($source);
                if (!$source)
				{
                    continue 2;
                }
                $page_token = $data['access_token'];
                $post = $this->getDataSource($source['page_fbid'], $page_token);

                $randomPost = $post[array_rand($post, 1)];

                $checkPost = $this->db->where('parent_post_id', $randomPost['id'])
                    ->where('page_id', $data['page_id'])
                    ->get('result_repost')->row_array();

            } while(empty($checkPost) == false);


            if ($randomPost['type'] == 'photo') {
                $param = array(
                    'access_token' => $data['access_token'],
                    'url' => $randomPost['full_picture'],
                    'caption' => $randomPost['message']."\n". $setup['tag'],
                );

                $path = $data['page_fbid'] . '/photos';

            } elseif ($randomPost['type'] == 'video') {

                $param = array(
                    'access_token' => $data['access_token'],
                    'file_url' => $randomPost['source'],
                    'description' => isset($randomPost['message'])? $randomPost['message']."\n". $setup['tag'] : '',
                );

                $path =  $data['page_fbid'].'/videos';

            }else{

                   $param = array(
                    'access_token' => $data['access_token'],
                    'message' => $randomPost['message']."\n". $setup['tag'],
               	 );

                $path = $data['page_fbid'] . '/feed';
            }
            $post_comment = $this->curl->fb_api($path, 'POST', $param);
			if($test)
			{
				echo 'paranm';
				print_r($randomPost);
				print_r($param);
				print_r($post_comment);
			}
					

            // success
            $get_time = $this->get_time($setup['delay_post'], $setup['execute_time'],1);
            if (isset($post_comment['id'])) {

                $this->db->where('repost_id', $data['repost_id'])->set(array('result' => $data['result'] +1,
                    'result_day' => $data['result_day'] +1, 'time_run' => $get_time['nexttime']))
                    ->update('repost');

                // insert result repost
				$insertResultRepost = array(
					 'page_id' => $data['page_id'],
                     'content' => isset($randomPost['message']) ? $randomPost['message'] :'',
					 'link'    => isset($randomPost['link']) ? $randomPost['link'] : '',
                     'picture' => isset($randomPost['full_picture']) ? $randomPost['full_picture'] :'',
					 'time_posted' => time(),
					 'parent_post_id' => $randomPost['id']
				);
              
                $this->db->insert('result_repost', $insertResultRepost);

            } else {

                // error
				$log = array(
					'mess_error' => $post_comment['error']['code'].' - '.$post_comment['error']['message'],
					'action'	=> 'repost',
					'page_id'	=> $data['page_id'],
					'user_id'	=> $user_id,
					'time_log'	=> time()
				);
                $this->db->insert('logs', $log);

            }
        }
    }

    private function getDataSource($page_id, $page_token) {

        $path = $page_id . '/feed?fields=full_picture,picture,message,link,source,type&access_token='.$page_token;
        $post = $this->curl->fb_api($path);
        if (isset($post['data']))
            return $post['data'];
        else
            exit();

    }

    private function get_time($delay, $exec_time, $type)
    {
        $timetoday = strtotime('TODAY');

        $start = $timetoday + $exec_time[0] * 3600;

        $end = $timetoday + $exec_time[1] * 3600;

        if ($end < $start) {

            if (intval(date('H')) < $exec_time[0]) {
                $start -= 86400;
            } else {
                $end += 86400;
            }

        }

        if ($type == 1) {

            $delay = rand($delay[0], $delay[1]) * 60;

            $nexttime = $delay + time();

            $new_day = false;

            if ($nexttime >= $end) {

                $nexttime = $start + 86400;
                $new_day = true;
            }

            return array('nexttime' => $nexttime, 'new_day' => $new_day);
        } else {

            $nexttime = $start + 86400;
            return $nexttime;
        }

    }

}
