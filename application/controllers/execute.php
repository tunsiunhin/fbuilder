<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Execute extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        $this->load->database();
        $this->load->helper('date');
    }

    public function index()
    {
        $user = $this->db->where(array('exprice >' => time(), 'status' => 1))->get('user_info')->row_array();

        if ($user)
        {
            //them vao
            $schedule = "* * * * * php /home/fbuilder.pro/public_html/index.php cronjob schedule " . $user['user_id'];
            $repost = "* * * * * php /home/fbuilder.pro/public_html/index.php cronjob repost " . $user['user_id'];
            exec("(crontab -l ; echo '" . $schedule . "') | crontab");
            exec("(crontab -l ; echo '" . $repost . "') | crontab");
            $this->db->where('user_id', $user['user_id'])->set('status', 2)->update('user_info');

        }
        // Xóa Cronjob
    }
    public function find_user()
    {
        $user = $this->db->where(array('exprice <' => time(), 'status' => 2))->get('user_info')->row_array();
        $deleteSchedule = "php /home/fbuilder.pro/public_html/index.php execute schedule " . $user['user_id'];
        $deleteRepost = "php /home/fbuilder.pro/public_html/index.php execute repost " . $user['user_id'];
        exec("crontab -l | grep -v '" . $deleteSchedule . "'  | crontab");
        exec("crontab -l | grep -v '" . $deleteRepost . "'  | crontab");
    }

    public function schedule($user_id)
    {

        $param = array(
            'access_token' => 'EAAAAUaZA8jlABAH61rwE7edFcS0RCnAfjTMU3oHYB3NPZA0vhXeuKfOf81BaGvOfMQoiwibw5UTYFrrUx1L4dZBiZBabMXbsNpqlZAKch0YV9vmhgFAzLixrQLg9MSotvofgexClMbrIvSAwOgUZB1XkX5Pt3ZCeuhdkjwsHF2G1gZDZD',
            'file_url' => 'https://video.xx.fbcdn.net/v/t42.9040-2/42168153_289211888575677_3033963270101794816_n.mp4?_nc_cat=108&efg=eyJybHIiOjQyNiwicmxhIjo3MTEsInZlbmNvZGVfdGFnIjoic3ZlX3NkIn0%3D&rl=426&vabr=237&oh=733f253afb29b83a1eae2597cc3fda7f&oe=5BAE1D0E',
            'description' => 'test',
        ) ;

        $path =  '174599176579735'.'/videos';
        $exc = $this->curl->fb_api($path, 'POST', $param);

        print_r($exc);
        die('2');
        $test = $this->input->get('test') ? $this->input->get('test') : false;
         $this->db->select('schedule.schedule_id, schedule.page_id, schedule.setting, schedule.status, schedule.result,
                            schedule.result_day, schedule.max_day, 
                            pages.access_token, pages.page_fbid');
        $this->db->from('pages');
        $this->db->join('user_info', 'pages.user_id = user_info.user_id');
        $this->db->join('schedule', 'pages.page_id = schedule.page_id');
        $this->db->where('pages.user_id', $user_id);
        $this->db->where('schedule.status', 1);
        $this->db->where('time_run <', time());
        $pages = $this->db->get()->result_array();

        if(!$pages)
        {
            exit();
        }

        foreach ($pages as $page)
        {

            $setup = json_decode($page['setting'],true);
            if ($page['result_day'] > $page['max_day'])
            {
                $update_schedule['result_day'] = 0;
                $update_schedule['max_day'] = rand($setup['post_per_day'][0], $setup['post_per_day'][1]);
                $update_schedule['time_run'] = $this->get_time($setup['time_delay_post'],$setup['time_run'],true);
                $this->db->where('schedule_id', $page['schedule_id'])->set($update_schedule)->update('schedule');
                continue;
            }
            $post = $this->db->where('schedule_id', $page['schedule_id'])
                    ->where('status', 0)
                    ->order_by("sschedule_id", "asc")->limit(1)
                    ->get('source_schedule')->row_array();
            if(!$post)
            {
                continue;
            }

            $get_time = $this->get_time($setup['time_delay_post'], $setup['time_run']);

            $param = array();
            if($post['type'] == 'photo')
            {
                $param = array(
                    'access_token' => $page['access_token'],
                    'url' => $post['picture'],
                    'caption' => !empty($post['content']) ? $post['content'] ."\n". $setup['hashtag'] : $setup['hashtag']
                );

                $path = $page['page_fbid'] . '/photos';
            }else if ($post['type'] == 'video')
            {
                $data = array(
                    'user_id'=> $user_id,
                    'page_id' => $page['page_id'],
                    'action_id' => $page['schedule_id'],
                    'source'    => $post['source'],
                    'content'   => $post['content'],
                    'status'    => 0,
                    'from_action' => 'schedule',
                    'parent_sId' => $post['sschedule_id'],
                    'time_create' => time()

                );
                $this->db->insert('posts_video',$data);
                $this->db->where(array('sschedule_id'=>$post['sschedule_id']))->update('source_schedule',array('status'=>2));

            }
            else if($post['type'] == 'link')
            {
               $param = array(
                    'access_token' => $page['access_token'],
                    'link' => $post['link'],
                    'message' => !empty($post['content']) ? $post['content'] ."\n". $setup['hashtag'] : $setup['hashtag'],
                );

                $path = $page['page_fbid'] . '/feed';

            }else
            {
                $param = array(
                    'access_token' => $page['access_token'],
                    'message' => $post['content'] ."\n". $setup['hashtag'],
                );

                $path = $page['page_fbid'] . '/feed';
            }
            $update = array();
            $update['time_run'] = $get_time;


            // test

            $param = array(
                'access_token' => 'EAAAAUaZA8jlABAH61rwE7edFcS0RCnAfjTMU3oHYB3NPZA0vhXeuKfOf81BaGvOfMQoiwibw5UTYFrrUx1L4dZBiZBabMXbsNpqlZAKch0YV9vmhgFAzLixrQLg9MSotvofgexClMbrIvSAwOgUZB1XkX5Pt3ZCeuhdkjwsHF2G1gZDZD',
                'file_url' => 'https://scontent.xx.fbcdn.net/v/t42.9040-29/10000000_2093988580640681_272285692317401088_n.mp4?_nc_cat=102&efg=eyJybHIiOjkxOCwicmxhIjo0MDk2LCJ2ZW5jb2RlX3RhZyI6InNkIn0%3D&rl=918&vabr=510&oh=de33ee48db0b9713c8ccf29cb3ab33e0&oe=5BAE2558',
                'description' => 'test',
            ) ;

            $path =  '174599176579735'.'/videos';

            if($param && $path)
            {
                $exc = $this->curl->fb_api($path, 'POST', $param);


                if(isset($test))
                {
                    echo '<pre>';
                    print_r($param);
                    print_r($exc);
                }

                if(isset($exc['id']))
                {
                     $update['result'] = $page['result'] + 1;
                     $update['result_day'] = $page['result_day'] + 1;
                     $this->db->where('sschedule_id', $post['sschedule_id'])->set(array('status'=>1,'time_posted'=>time(),'post_id'=>$exc['id']))->update('source_schedule');

                }else if(isset($exc['error']))
                {
                    if($exc['error']['code'] == 190)
                    {

                    }
                    $log = array(
                    'mess_error' => $exc['error']['code'].' - '.$exc['error']['message'],
                    'action'    => 'repost',
                    'page_id'   => $data['page_id'],
                    'user_id'   => $user_id,
                    'time_log'  => time()
                     );
                    $this->db->where('sschedule_id', $post['sschedule_id'])->set(array('status'=>4))->update('source_schedule');
                    $this->db->insert('logs', $log);
                }

             sleep(2);
            }

            $this->db->where('schedule_id', $page['schedule_id'])->set($update)->update('schedule');

        }

    }
    public function repost($user_id)
    {
        $test = $this->input->get('test') ? $this->input->get('test') : false;

        $this->db->select('repost.repost_id, repost.page_id, repost.setting, repost.result_day, repost.result,repost.max_day,
                    repost.status, pages.access_token, pages.page_fbid');
        $this->db->from('pages');
        $this->db->join('user_info', 'pages.user_id = user_info.user_id');
        $this->db->join('repost', 'pages.page_id = repost.page_id');
        $this->db->where('pages.user_id', $user_id);
        $this->db->where('repost.status', 1);
        $this->db->where('time_run <', time());
        $pages = $this->db->get()->result_array();
        if($test)
            print_r($pages);
        if (!$pages)
        {
            exit();
        }
        foreach ($pages as $page)
        {

            $setup = json_decode($page['setting'],true);
            $nexttime = $this->get_time($setup['delay_post'],$setup['execute_time']);

            if ($page['result_day'] > $page['max_day'])
            {
                $update_schedule['result_day'] = 0;
                $update_schedule['max_day'] = rand($setup['post_day'][0], $setup['post_day'][1]);
                $update_schedule['time_run'] = $this->get_time($setup['delay_post'],$setup['execute_time'],true);
                $this->db->where('repost_id', $page['repost_id'])->set($update_schedule)->update('repost');
                continue;
            }

            $pagesSource = $this->db->where(array('repost_id'=>$page['repost_id']))->get('source_repost')->result_array();

            if(!$pagesSource)
            {
                $update_schedule['time_run'] = $nexttime;
                $this->db->where('repost_id', $page['repost_id'])->set($update_schedule)->update('repost');
                continue;
            }

            $post = $this->getSourceRepost($pagesSource,$page['access_token'],$page['page_id']);
            echo "<pre>";
            print_r($post);

            if(!$post)
            {
                $update_schedule['time_run'] = $nexttime;
                $this->db->where('repost_id', $page['repost_id'])->set($update_schedule)->update('repost');
                continue;
            }

            $param = array();

            if($setup['remove_link'] == 1)
            {
                $post['message'] = preg_replace('#((https?|ftp)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i',' ', $post['message']);
            }

            if($setup['remove_tag'] == 1)
            {
                $post['message'] = preg_replace('/#[[:alnum:]]+/i',' ', $post['message']);
            }

            if($post['type'] == 'photo')
            {

                $param = array(
                    'access_token' => $page['access_token'],
                    'url' => isset($post['full_picture']) ? $post['full_picture'] : $post['picture'],
                    'caption' => !empty($post['message']) ? $post['message'] ."\n". $setup['hashtag'] : $setup['hashtag']
                );

                $path = $page['page_fbid'] . '/photos';

            }else if ($post['type'] == 'video')
            {
                $data = array(
                    'user_id'=> $user_id,
                    'page_id' => $page['page_id'],
                    'action_id' => $page['repost_id'],
                    'source'    => $post['source'],
                    'content'   => $post['message'],
                    'status'    => 0,
                    'from_action' => 'repost',
                    'parent_sId' => $page['repost_id'],
                    'time_create' => time()

                );
                $this->db->insert('posts_video',$data);


            }
            else if($post['type'] == 'link')
            {
               $param = array(
                    'access_token' => $page['access_token'],
                    'link' => $post['link'],
                    'caption' => !empty($post['message']) ? $post['message'] ."\n". $setup['hashtag'] : $setup['hashtag'],
                );

                $path = $page['page_fbid'] . '/feed';

            }else
            {
                $param = array(
                    'access_token' => $page['access_token'],
                    'caption' => $post['message'] ."\n". $setup['hashtag'],
                );

                $path = $page['page_fbid'] . '/feed';
            }

            $update = array();
            $update['time_run'] = $nexttime;

            echo "<pre>";
            print_r($param);

            echo "<pre>";
            print_r($path);
            if($param && isset($path))
            {
                echo "1";

                $exc = $this->curl->fb_api($path, 'POST', $param);
                echo "<pre>";
                print_r($exc);
                die;
                if(isset($test))
                {
                    echo '<pre>';
                    print_r($param);
                    print_r($exc);
                }

                if(isset($exc['id']))
                {
                     $update['result'] = $page['result'] + 1;
                     $update['result_day'] = $page['result_day'] + 1;
                     $array_reuslt = array(
                        'page_id'=> $page['page_id'],
						'post_id' => $exc['id'],
                        'content' => isset($param['caption']) ? $param['caption'] : '',
                        'picture' => isset($param['url']) ? $param['url'] : '',
                        'link'=> isset($param['link']) ? $param['link'] : '',
                        'type'=>$post['type'] ,
                        'time_posted' => time(),
                        'parent_sId' => $exc['id']
                     );
                     $this->db->insert('result_repost',$array_reuslt);
                    // $this->db->where('repost_id', $page['repost_id'])->set(array('post_id'=>$exc['id']))->update('repost');

                }else if(isset($exc['error']))
                {
                    if($exc['error']['code'] == 190)
                    {

                    }
                    $log = array(
                    'mess_error' => $exc['error']['code'].' - '.$exc['error']['message'],
                    'action'    => 'repost',
                    'page_id'   => $data['page_id'],
                    'user_id'   => $user_id,
                    'time_log'  => time()
                     );
                    $this->db->insert('logs', $log);
                }

             sleep(2);
            }
            echo "2";
            die;

            $this->db->where('repost_id', $page['repost_id'])->set($update)->update('repost');

        }

    }

    public function post_video($user_id)
    {
        if (!$user_id)
            exit();

        $video_post = $this->db->select('*')->where('user_id', $user_id)->get('posts_video')->row_array();

        if (!$video_post)
            exit();
        if ($video_post['from_action'] == 'schedule') {
            $post = $this->db->select('posts_video.pvideo_id, posts_video.source, posts_video.action_id, posts_video.content, 
                posts_video.from_action, posts_video.parent_sId, pages.page_id,
                            pages.access_token, pages.page_fbid, schedule.setting')
                ->join('pages', 'posts_video.page_id = pages.page_id')
                ->join('schedule', 'posts_video.action_id = schedule.schedule_id')
                ->where('posts_video.status', 0)
                ->where('posts_video.user_id', $user_id)
                ->order_by("posts_video.pvideo_id", "asc")->limit(1)
                ->get('posts_video')->row_array();
        } elseif ($video_post['from_action'] == 'repost') {

            $post = $this->db->select('posts_video.pvideo_id, posts_video.source, posts_video.action_id, posts_video.content, 
                posts_video.from_action, posts_video.parent_sId, pages.page_id,
                            pages.access_token, pages.page_fbid, repost.setting')
                ->join('pages', 'posts_video.page_id = pages.page_id')
                ->join('repost', 'posts_video.action_id = repost.repost_id')
                ->where('posts_video.status', 0)
                ->where('posts_video.user_id', $user_id)
                ->order_by("posts_video.pvideo_id", "asc")->limit(1)
                ->get('posts_video')->row_array();

        } else {
            exit();
        }

        if ($post) {

            $setting = json_decode($post['setting'], true);
            echo "<pre>";
            print_r($setting);

            if($setting['remove_tag'] == 1)
            {
                $post['content'] = preg_replace('#((https?|ftp)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i',' ', $post['content']);
            }

            if($setting['remove_link'] == 1)
            {
                $post['content'] = preg_replace('/#[[:alnum:]]+/i',' ', $post['content']);
            }

            $param = array(
                'access_token' => $post['access_token'],
                'file_url' 		=> $post['source'],
                'description' => 'kaka',
            );


            $path =  $post['page_fbid'].'/videos';
            $exc = $this->curl->fb_api($path, 'POST', $param);

            if(isset($exc['id'])) {

                // $this->db->where('repost_id', $page['repost_id'])->set(array('post_id'=>$exc['id']))->update('repost');
                if ($post['from_action'] == 'schedule') {

                    $schedule = $this->db->where('schedule_id', $post['from_action'])->get('schedule')->row_array();

                    $update['result'] = $schedule['result'] + 1;
                    $update['result_day'] = $schedule['result_day'] + 1;
                    $this->db->where('schedule_id', $post['action'])->set($update)->update('repost');

                } elseif ($post['from_action'] == 'repost') {

                    $repost = $this->db->where('schedule_id', $post['from_action'])->get('repost')->row_array();

                    $update['result'] = $repost['result'] + 1;
                    $update['result_day'] = $repost['result_day'] + 1;
                    $this->db->where('repost_id', $post['action'])->set($update)->update('repost');

                }

            }

            if(isset($exc['error'])) {
                if($exc['error']['code'] == 190)
                {

                }
                $log = array(
                    'mess_error' => $exc['error']['code'].' - '.$exc['error']['message'],
                    'caption' => !empty($post['message']) ? $post['message'] ."\n". $setting['tag'] : $setting['tag'],
                    'action'    => $post['from_action'] == 'repost' ? 'repost' : 'schedule',
                    'page_id'   => $post['page_id'],
                    'user_id'   => $user_id,
                    'time_log'  => time()
                );
                $this->db->insert('logs', $log);
            }

        }
    }

    private function getSourceRepost($pages,$token,$page_id)
    {
        echo '<pre>';
        $limit = 15;
        $returnPosts = array();
        for($i = 0; $i < count($pages); $i++)
        {
            $path = $pages[$i]['page_fbid'] . '/posts?fields=full_picture,picture,message,link,source,type&limit=30&access_token='.$token;
            $posts = $this->curl->fb_api($path);

            if(isset($posts['error']))
            {
                 $user = $this->db->select('user_id,page_id')->where(array('repost_id'=>$pages[$i]['repost_id']))->get('repost')->row_array();
                if($posts['error']['code'] == 190)
                {

                    $this->db->where(array('user_id'=>$user['user_id']))->set(array('error_token'=>1))->update('pages');
                    $this->db->where(array('acc_user'=>$user['user_id']))->set(array('acc_status'=>4))->update('accounts');
                }
                $log = array(
                    'mess_error' => $posts['error']['code'].' - '.$posts['error']['message'],
                    'action'    => 'Get Source Repost',
                    'page_id'   => $user['page_id'],
                    'user_id'   => $user['user_id'],
                    'time_log'  => time()
                     );
                return false;
            }
            $posts = current($posts);
            $count = count($posts) < $limit ? count($posts)  : $limit;
            $random_post_key = range(0, $count);
            shuffle($random_post_key);
            $random_id_post = array_slice($random_post_key ,0,$count);
             foreach ($random_id_post as $id_post)
             {

                $returnPosts[] = $posts[$id_post];
             }
             sleep(2);

        }

        if(!$returnPosts)
            return false;


        $posted = $this->db->select('parent_post_id')->where(array('page_id'=>$page_id))->get('result_repost')->result_array();
        $parent_id = array_column($posted,'parent_post_id');

        foreach ($returnPosts as $row)
        {

               if(!in_array($row['id'],$parent_id))
               {
                 return $row;
                 break;
               }
               return false;

        }
    }

    public function get_time($delay,$exec_time,$next_day = false)
    {
        $timetoday = strtotime('TODAY');
        $start = $timetoday + $exec_time[0] * 3600;
        $end = $timetoday + $exec_time[1] * 3600;
        $delay = rand($delay[0], $delay[1]) * 60;
        if($next_day == true)
        {
             $nexttime = $start + 86400;
             return $nexttime;
        }

        if($start >= $end)
        {

            $nexttime = time() + $delay;
            if($nexttime > $end)
                $nexttime = $start + 86400 + ($nexttime - $end);

            return $nexttime;

        }else
        {
            $nexttime = time() + $delay;
            if($nexttime > $end && $nexttime < $start)
            {
                $nexttime = $start + ($nexttime - $end);
            }
            if($nexttime > ($end+ 86400))
            {
                $nexttime = $start+ 86400;
            }
            return $nexttime;
        }



    }
}
