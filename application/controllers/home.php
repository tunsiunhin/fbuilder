<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('main_model');
		$this->data['panel_notify'] = array();
		if(!$this->accounts)
		{
			$this->data['panel_notify'][]	 = $this->load->view('layout/notify_token','',true);
		}
		$q = $this->input->get('search') ? $this->input->get('search') : '';
		$this->data['pages'] = $this->main_model->getPages($this->user_id,$q);
		$this->view('dashboard');
	}
	public function repost()
	{
		$this->load->model('repost_model');
		$this->data['all_page']= $this->repost_model->getPage($this->user_id);

		$page_ids = array_column($this->data['all_page'], 'page_id');

		$page_id=  $this->input->get('page_id');

		if(in_array($page_id, $page_ids))
		{
			$repost = $this->repost_model->getRepost($page_id);
			$result_repost = $this->repost_model->getResultRepost($page_id);

			if(empty($repost['repost_id']))
			{
				$repost['repost_id'] = 0;
				$repost['page_id'] = $page_id;
				$repost['setting'] = '{"post_day":["0","0"],"delay_post":["0","0"],"execute_time":["0","0"],"hastag":"","remove_tag":0,"remove_link":0}';
				$repost['status'] = 0;
				$repost['result'] = 0;
			}
			$data['repost'] = $repost;
			$this->data['repost'] = $repost;
			$data['result'] = $result_repost;
			$this->data['layout'] = $this->load->view('layout/repost_view', $data, true);
		}else
		{
			$repost['name'] = 'N/a';
			$this->data['repost'] = $repost;
			$this->data['layout'] = $this->load->view('layout/select_account','',true);
		}
		
		$this->view('repost');
		
	}
	
	public function schedule()
	{
		$page_id = $this->input->get('page_id');
		$this->load->model('schedule_model');
		$this->data = $this->schedule_model->getInfo($page_id,$this->user_id);
		if($this->data['schedule'])
		{
			$this->data['layout'] = $this->load->view('layout/schedule_layout',$this->data,true);
		}else
		{
			$this->data['layout'] = $this->load->view('layout/select_account','',true);
		}
		
		$this->view('schedule');
	
	}

	public function upload(){
		$this->view('upload');
	}

	public function guilde(){
		$this->view('guilde');
	}

	public function pricing(){
		$this->view('pricing');
	}

}
