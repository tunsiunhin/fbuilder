<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        $this->load->helper('response');
    }
    public function token()
    {
        $limit = 1;
    	$token = $this->input->post('token');
    	if(!$token)
        {
            echo response(400,'Error Param');
            exit();
        }
    		
    	$max = 1;
    	$accounts = $this->db->where(array('acc_user'=>$this->user_id))->get('accounts')->result_array();
        $path = 'me?fields=accounts{name,access_token,id,fan_count,picture},id,name,picture&access_token='.$token;

        $res = $this->curl->fb_call($path);
        if(isset($res['error']))
        {
            echo response(400,'Token Error : '.$res['error']['message'].' - Code :'.$res['error']['code']);
            exit();
        }
    	if(!$accounts)
    	{
    		$this->add_account($res,$token);
    		
    	}else
        {
            $ids = array_column($accounts, 'acc_fbid');
            if(in_array($res['id'],$ids))
            {
                $ud_account = array('acc_name'=>$res['name'],'acc_token'=>$token,'acc_status'=>1);
                $ud_pages   = array();
                if(!empty($res['accounts']['data']))
                {
                    foreach ($res['accounts']['data'] as $row)
                    {
                        $ud_pages[] = array(
                            'access_token'=>$row['access_token'],
                            'page_fbid'   => $row['id'],
                            'name'        =>$row['name'],
                            'picture'    => 'https://graph.facebook.com/'.$row['id'].'/picture',
                            'fan_count'  => $row['fan_count'],
                            'error_token'   => 0
                        );  
                    }
					$this->db->update_batch('pages',$ud_pages,'page_fbid');
                }
                $this->db->where('acc_fbid',$res['id'])->update('accounts',$ud_account);
                echo response(200,'Success');
                exit();
            }else
            {
                if(count($accounts) < $limit)
                {
                    $this->add_account($res,$token);
                }else
                {
                    echo response(199,'Đã Đạt Giới Hạn Thêm Tài Khoản');
                    exit();
                }

            }
        }
    		
    }
    public function add_account($res,$token)
    {
        if($res['id'])
        {
                $account = array(
                    'acc_user'=> $this->user_id,
                    'acc_name'=> $res['name'],
                    'acc_fbid'=>$res['id'],
                    'acc_token'=> $token,
                    'acc_picture'=> 'https://graph.facebook.com/'.$res['id'].'/picture',
                    'acc_status' =>1
                );

                $this->db->insert('accounts',$account);
                $account_id = $this->db->insert_id();

                if(!empty($res['accounts']['data']))
                {
                    foreach ($res['accounts']['data'] as $row)
                    {
                        $pages[] = array(
                            'access_token'=>$row['access_token'],
                            'account_id'  => $account_id,
                            'user_id'     => $this->user_id,
                            'page_fbid'   => $row['id'],
                            'name'        =>$row['name'],
                            'picture'    => 'https://graph.facebook.com/'.$row['id'].'/picture',
                            'fan_count'  => $row['fan_count'],
                            'error_token'   => 0
                        );  
                    }
                    $this->db->insert_batch('pages',$pages);
                    echo response(200,'Success');
                    exit();
                }
        }else
        {
            echo response(199,'Đã có lỗi.Vui lòng liên hệ admin');
            exit();
        }
    }
    

}
