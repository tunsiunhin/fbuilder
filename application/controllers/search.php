<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
class Search extends MY_Controller
{
	public $token;
	public function __construct(){
   		parent::__construct();
   		$this->load->helper('response');
   		$this->load->library('curl');
   		$this->token = current($this->accounts)['acc_token'];
   	}
   	public function index()
   	{
   		$type = $this->input->get('type');
   		if(!$type)
   				$type = 'facebook';
   		
   		switch ($type) {
   			case 'facebook':
   				$this->facebook_search();
   				break;
   			case 'facebook_feed':
   				$this->facebook_search_feed();
   				break;
   			case 'instagram';
   				$this->instagram_search();
   				break;
            case 'instagram_feed':
               $this->instagram_search_feed();
               break;
   			case 'pinterest':
   				$this->pinterest_search();
   				break;
			   case 'fb_repost':
   				$this->fb_repost();
   				break;
   			default:
   				# code...
   				break;
   		}
   	}
   	private function facebook_search()
   	{
   		
     	$q = $this->input->get('q');
   		if(!$q)
   		exit();
		$q = str_replace(' ','+',$q);
		$path = 'search?type=page&limit=100&fields=id,name,picture,fan_count&q='.$q.'&access_token='.$this->token;
		
		$res = $this->curl->fb_call($path);
		if(isset($res['error']) || isset($res['error']['code']) == '190')
		{
			echo response(400,$res['error']['message']);	
			exit();
		}
		$result = array(
			'code' => 200,
			'html' => $this->load->view('layout/facebook_sub_search',$res,true)
		);
		echo json_encode($result,true);

   	}
   	private function facebook_search_feed()
   	{
   		$page_id = $this->input->get('page_id');
         $cursor  = $this->input->get('cursor') ? $this->input->get('cursor') : '';
         if($cursor)
            $cursor = '&after='.$cursor;
<<<<<<< HEAD
   		$path = $page_id.'/posts?fields=id,message,picture,full_picture,reactions.limit(0).summary(1),comments.limit(0).summary(1),shares,type,source,link&limit=30'.$cursor.'&access_token='.$this->token;
=======
   		$path = $page_id.'/posts?fields=id,message,picture,full_picture,reactions.limit(0).summary(1),comments.limit(0).summary(1),shares,type,link,source&limit=30'.$cursor.'&access_token='.$this->token;
>>>>>>> 401d9b8779d710933aed08e7025b617a4104c8b2
   		$res = $this->curl->fb_call($path);
   		if(isset($res['error']) || isset($res['error']['code']) == '190')
   		{
   			echo response(400,$res['error']['message']);	
   			exit();
   		}
		$result = array(
			'code' => 200,
			'html' => $this->load->view('layout/facebook_feed_search',$res,true),
         'cursor' => isset($res['paging']['cursors']['after']) ? $res['paging']['cursors']['after'] :''
		);
		echo json_encode($result);
   	}
      public function instagram_search()
      {
         $q = $this->input->get('q');
         if(!$q)
            exit();
         $url = 'https://www.instagram.com/web/search/topsearch/?context=blended&query='.$q;
         $res = $this->curl->call($url);

         if(!$res)
         {
            exit();
         }
         $res = json_decode($res,true);
		 $cusor = isset($res['user']['media']['page_info']['end_cursor']) ? $res['user']['media']['page_info']['end_cursor'] : "";
         $result = array(
         'code' => 200,
         'html' => $this->load->view('layout/instagram_sub_search',$res,true),
		 'cursor'=>$cusor
         );
         echo json_encode($result);

      }
      private function instagram_search_feed()
      {
         $user = $this->input->get('username');
		 $cursor = $this->input->get('cursor') ? $this->input->get('cursor') : '';
		 $pk  = $this->input->get('pk');
         if(!$pk)
            exit();
		 if(!$cursor)
		 {
			$path = 'https://www.instagram.com/'.$user.'/?__a=1';	
		 }else
		 {
			 $path = 'https://www.instagram.com/graphql/query/?query_hash=a5164aed103f24b03e7b7747a2d94e3c&variables={"id":"'.$pk.'","first":12,"after":"'.$cursor.'"}';	
		 }

         $res = $this->curl->call($path,'','','FJlaXbtCbo.txt');
         if(!$res)
            exit();
		
         $res = json_decode($res,true);
		 if(isset($res['graphql']['user']['edge_owner_to_timeline_media']['edges']))
		 {
		 	$data['data'] = $res['graphql']['user']['edge_owner_to_timeline_media']['edges'];
			$cursor		  = $res['graphql']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor'];
              $result = array(
                  'code' => 200,
                  'html' => $this->load->view('layout/instagram_feed_search',$data,true),
				  'cursor' => $cursor
               );
		 }
         if(isset($res['data']['user']['edge_owner_to_timeline_media']['edges']))
         {
              $data['data'] = $res['data']['user']['edge_owner_to_timeline_media']['edges'];
              $result = array(
                  'code' => 200,
                  'html' => $this->load->view('layout/instagram_feed_search',$data,true),
				  'cursor' => $cursor
               );
         }
       
         echo json_encode($result);
      }
      public function pinterest_search()
      {
         $q = $this->input->get('q');
         $cursor =  $this->input->get('cursor') ? $this->input->get('cursor') : '' ;
         $header = array(
            'Accept: application/json, text/javascript, */*, q=0.01',
            'Accept-Encoding: utf-8',
            'Accept-Language:vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2',
            'Connection:keep-alive',
            'Host: www.pinterest.com',
            'Referer: https://www.pinterest.com/',
            'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) Chrome/61.0.3163.100 Safari/537.36',
            'X-APP-VERSION: 51031ef',
            'X-Pinterest-AppState: background',
            'X-Requested-With:XMLHttpRequest'
        );
         if($cursor)
         {

         }else
         {
            $url ='source_url=/search/pins/?q='.$q.'&rs=typed&term_meta[]='.$q.'|typed';
            $path = 'data={"options":{"bookmarks":[""],"query":"'.$q.'","scope":"pins"},"context":{}}';
         }
         $res   = $this->curl->pinterest_call($url,$path);
         if($res)
         {
          /*  echo '<pre>';*/
            $res = json_decode($res,true);
     /*       print_r($res);die;*/
            $data = $res['resource_data_cache'][0];
            $result = array(
                  'code' => 200,
                  'html' => $this->load->view('layout/pinterest_feed',$data,true)
            );
            echo json_encode($result);
         }else
         {
            echo reponse(400,'Error');
            exit();
         }
      }
	  public function fb_repost(){
         $key = $this->input->post('keyword');
         $token = $this->accounts[0]['acc_token'];
         $path = 'search?q='.$key.'&limit=100&type=page&fields=name,picture,fan_count&access_token='.$token;
         $pages = $this->curl->fb_call($path);
         $html = '';
         foreach ($pages['data'] as $row) {
            $html .= '<li data-key="'.$row['id'].'">'
                . '<div class="hidden">'.json_encode($row).'</div>'
                . '<img src="'.$row['picture']['data']['url'].'" />'
                . '<div>'
                . '<label><a target="_blank" href="https://www.facebook.com/'.$row['id'].'">'.$row['name'].'</a></label>'
                . '<span>'.number_format($row['fan_count']).' Likes</span>'
                . '</div></li>';
         }
         echo $html;
      }

}
?>