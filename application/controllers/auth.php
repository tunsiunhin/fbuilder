<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
class Auth extends CI_Controller
{
	public function __construct(){
   		parent::__construct();
   	}


	public function index()
	{	
		
		session_start();
		
		$auth_token = 'i1eZRPef5QEmyBfWM1Tx';
		
		$info  = $this->input->get('info');
		
		$token = $this->input->get('token');
		
		$verify = sha1($auth_token.$info);
		
		if(strcmp($token,$verify) == 0)
		{
			$user_info = json_decode(base64_decode($info),true);
			
		/*	if($user_info['expire'] < time() - 1800){
				echo response(401,'Unauthorized');
			}*/
			
			$user_id = intval($user_info['user_id']);
			$user_email = $user_info['user_email'];

			$user = $this->db->where('user_id',$user_id)->get('user_info')->row_array();

			$_SESSION['user_id']     = $user_id;
			$_SESSION['user_email']  = $user_email;
			$_SESSION['exprice'] 	= !$user['exprice'] ? $user['exprice'] : 0 ;
			if(!$user)
			{
				
				$user = array(
					'user_id'     => $user_id,
					'exprice'	  =>0
				);
				
				$this->db->insert('user_info',$user);
				$accounts = array();

			}else
			{
				$accounts = $this->db->select('acc_token,acc_status,id')->where('acc_user',$user_id)->get('accounts')->result_array();
			}
				
			$_SESSION['account'] = $accounts;
			
			redirect(base_url());
		}
		else{
			echo response(401,'Unauthorized');
		}
		
	}
}
?>