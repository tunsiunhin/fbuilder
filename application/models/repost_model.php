<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Repost_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
 	}

 	public function getPage($user_id){
 		$page = $this->db->where('user_id', $user_id)->get('pages')->result_array();
 		return $page;
 	}

 	public function getRepost($page_id){
 		$this->db->select('repost.page_id, name, repost_id, setting, status, result,repost.time_run');
 		$repost = $this->db->join('pages', 'pages.page_id=repost.page_id', 'right')->where('pages.page_id', $page_id)->get('repost')->row_array();
 		if(empty($repost)){
 			return array();
 		}else{
	 		$source = $this->db->where('repost_id', $repost['repost_id'])->get('source_repost')->result_array();

	 		$repost['source'] = $source;
	 		return $repost;
 		}
 	}

 	public function getResultRepost($page_id){
 		$result = $this->db->where('page_id', $page_id)->order_by('rrepost','desc')->limit(100)->get('result_repost')->result_array();
 		return $result;
 	}

 	public function setStatus($repost_id, $status){
 		$c = $this->db->set('status', $status)->where('repost_id', $repost_id)->update('repost');
 		return $c;
 	}

 	public function addRepost($insert){
 		$this->db->insert('repost', $insert);
 		$id = $this->db->insert_id();
 		return $id;
 	}

 	public function updateRepost($id, $update){
 		$c = $this->db->where('repost_id', $id)->update('repost', $update);
 		return $c;
 	}

 	public function addSourceRepost($insert){
 		$c =  $this->db->insert('source_repost', $insert);
 		return $c;
 	}

 	public function removeSourceRepost($where){
 		$c = $this->db->where($where)->delete('source_repost');
 		return $c;
 	}
 }
 ?>