<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Main_model extends CI_Model{
	public function getPages($user_id,$q)
	{

		$this->db->select('pages.page_id,page_fbid,name,picture,fan_count,error_token,account_id,repost.result as repost_result,schedule.result as schedule_result,repost.status as repost_status,schedule.status as schedule_status');
		$this->db->join('repost','pages.page_id = repost.page_id','left');
		$this->db->join('schedule','pages.page_id = schedule.page_id','left');
		if($q)
			$this->db->like('pages.name',$q)->limit(30);
		$data = $this->db->where(array('pages.user_id'=>$user_id))->get('pages')->result_array();
		return $data;
	}	
}
?>