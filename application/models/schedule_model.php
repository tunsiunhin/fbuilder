<?php
class Schedule_model extends CI_Model{

	public function getInfo($page_id,$user_id)
	{
		$data = array();
		$this->db->select('name,page_id');
		$data['allpage'] = $this->db->where(array('user_id'=>$user_id))->get('pages')->result_array();
		$this->db->select('pages.name,pages.page_id,schedule.setting,schedule.schedule_id,schedule.time_run,schedule.status');
		$this->db->join('schedule','pages.page_id = schedule.page_id','left');
		$data['schedule'] = $this->db->where(array('pages.page_id'=>$page_id,'pages.user_id'=>$user_id))->get('pages')->row_array();
		if(isset($data['schedule']['schedule_id']))
		{
			$data['result'] = $this->db->where(array('schedule_id'=>$data['schedule']['schedule_id']))->limit(50)->order_by('schedule_id','desc')->get('source_schedule')->result_array();
		}else
		{
			$data['result'] = array();
		}
		return $data;
	}
	
}
?>