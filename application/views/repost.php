<div class="container">
    <div class="content-header clearfix">
        <div class="content-left">
            <i class="far fa-calendar-alt"></i>  Repost | <?= $repost['name']?>
        </div>
        <div class="content-right">
            <select class="selectpicker show-tick show-menu-arrow show_page" data-live-search="true" title="Select Page" data-size="10">
                <?php
                    foreach ($all_page as $page) {
                ?>
                <option value="<?= $page['page_id']?>" ><?= $page['name']?></option>
                <?php } ?>
            </select>

        </div>
    </div>
</div>
<?=$layout?>

<script type="text/javascript">
    $('.show_page').on('change', function(){
        var page_id = $(this).val();
        window.location.href = '/app/repost?page_id='+page_id;
    });
</script>
