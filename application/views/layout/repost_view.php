<?php
$setting = $repost['setting'];
$setting = json_decode($setting);
$post_day = $setting->post_day;
$execute_time = $setting->execute_time;
$delay_post = $setting->delay_post;
$remove_tag = $setting->remove_tag;
$remove_link = $setting->remove_link;
$tag = $setting->hashtag;
?>
<div class="container">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Setup </a></li>
      <li><a data-toggle="tab" href="#finded">Result</a></li>

      <li class="pull-right"><input type="checkbox" class="toggle-event" data-toggle="toggle" value="1" data-onstyle="success" data-size="larger" <?=$repost['status']==1?'checked':''?>></li>
      <li class="pull-right" style="margin-right: 10px;margin-top: 7px;">
        Next Run: <?php if($repost['time_run']){echo  date('H:i d/m/Y',$repost['time_run']);}else{echo 'N/A';}?>
      </li>
    </ul>
    
</div>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <div class="container setting-table">
        <div class="row">
            <div class="col-md-12">

                    <div class="panel panel-default ">
                    <div class="panel-heading"><i class="fas fa-pencil-alt"></i> Setting</div>
                    <div class="panel-body">
                        <form action="javascript:;">

                            <div>
                                <div class="fb-left">
                                    <span>Khoảng cách giữa các lần đăng bài (Phút)</span>
                                </div>
                                <div class="fb-right">
                                     <input class="form-control" id="delay0" type="number" min="0" value="<?= $delay_post[0]?>"  placeholder="From">
                                       <span>to</span>
                                     <input class="form-control" id="delay1" type="number" min="0" value="<?= $delay_post[1]?>"  placeholder="To">
                                </div>
                            </div>

                            <div>
                                <div class="fb-left">
                                    <span>Thời gian chạy</span>
                                </div>
                                <div class="fb-right">
                                     <input class="form-control" id="execute0" type="number" min="0" max="23" value="<?= $execute_time[0]?>"  placeholder="From">
                                       <span>to</span>
                                     <input class="form-control" id="execute1" type="number" min="0" max="23" value="<?= $execute_time[1]?>"  placeholder="To">
                                </div>
                            </div>

                            <div>
                                <div class="fb-left">
                                    <span>Số lượng bài post trên ngày</span>
                                </div>
                                <div class="fb-right">
                                     <input class="form-control" id="post0" type="number" min="0" value="<?= $post_day[0]?>"  placeholder="From">
                                       <span>to</span>
                                     <input class="form-control" id="post1" type="number" min="0" value="<?= $post_day[1]?>"  placeholder="To">
                                </div>
                            </div>

                            <div>
                                <div class="fb-left">
                                    <!-- <input type="checkbox" name=""> -->
                                    <span>Thêm tag mới</span>
                                </div>
                                <div class="fb-right">
                                     <input placeholder="#nguyenvuteam" id="tag" style="width: calc(100% - 19px);" value="<?= $tag?>" class="form-control" type="text">
                                </div>
                            </div>

                            <div>
                                <input type="checkbox" id="remove_tag" <?php if($remove_tag == 1) echo 'checked'?> name="">
                                <span>Xóa tag gốc</span>
                            </div>
                      
                            <div style="margin-top: 20px;">
                                <input type="checkbox" id="remove_link" <?php if($remove_link == 1) echo 'checked'?> name="">
                                <span>Xóa link gốc</span>
                            </div>

                            <div style="margin-top: 30px;">
                                <div class="pull-right" style="margin-right: 19px;">
                                    <button class="btn btn-success save"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Container  -->

    <div class="container source-table">
        <div class="panel panel-default ">
            <div class="panel-heading"><i class="fas fa-pencil-alt"></i> Source</div>
                <div class="panel-body">
        <div class="filter-acc-insta">
            <div class="dropdown">
                <i class="fa fa-search"></i>
                <button class="btn btn-success">Search</button>
                <input class="form-control dropdown-toggle search-acc-insta" type="search" data-toggle="dropdown" placeholder="Seach group in facebook" autocomplete="off" />
                
                <div class="ajax-acc-insta">
                    <div class="ai-content">
                        <ul class="select-acc-insta">
                          
                        </ul>
                    </div>
                    <div class="ai-loader text-center">
                        <i aria-hidden="true" hidden class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
                
                <ul class="dropdown-menu">
                   <li><a><i class="fa fa-search"></i> Press enter to Search</a></li>
                </ul>
            </div>
         </div>
         
         <div class="list-acc-insta">

            <ul>
                <?php
                    if(!empty($repost['source']))
                    {
                        foreach ($repost['source'] as $row) {
                            ?>
                            <li data-key="<?= $row['page_fbid']?>">
                                <img src="<?= $row['picture']?>">
                                <div>
                                    <label>
                                        <a href="" target="_blank"><?= $row['name']?></a>
                                    </label>
                                    <span><?= number_format($row['fan_count'])?> likes</span>
                                    <i class="fa fa-times"></i>
                                </div>
                            </li>
                            <?php
                        }
                    }
                  ?>
            </ul>  
           
         </div>
      
      </div> <!---End Body-->
        </div>
    </div><!-- Container  -->

  </div><!--  Home -->
  <div id="finded" class="tab-pane fade ">
    <div class="container">
        <div class="table-result">
               <table id="tab-result" class="table table-striped table-hover" style="width:100%;">
                    <thead>
                        <tr>
                            <th>Picture</th>
                            <th>Content</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($result as $row) {
                        ?>
                        <tr>
                            <td><a target="_blank" href="<?=isset($row['post_id'])?'https://fb.coom/'.$row['post_id'] : 'https://fb.com'?>"><img src="<?= $row['picture']?>" width="50" height="50"></a></td>
                            <td><?= $row['content']?></td>
                            <td><?= date("H:i D/M", $row['time_posted'])?></td>
                            <td><?=$row['type']?></td>
                            <td><button class="btn btn-default" ><i class="fa fa-eye"></i>View</button></td>
                        </tr>
                       <?php } ?>
                    </tbody>
                </table>
        </div>
    </div>
  </div>

</div><!-- Tabcontent -->
<script type="text/javascript">
    var page_id = <?=$repost['page_id']?>;
    var repost_id = <?= $repost['repost_id']?>;
    var status = <?= $repost['status']?>

$(document).ready(function(){
    $('#tab-result').DataTable();
    if(status == 0){
        $('#toggle-one').bootstrapToggle('off');
    }else{
        $('#toggle-one').bootstrapToggle('on');
    }
	
	$('.toggle').click(function(){
		
			var $this  = $(this).find('input');
			if($this.prop('disabled'))
			{
				return false;
			}
			var action_id = $this.val();
			var status    = ($this.is(':checked') ) ? 1 : 0;
			var params = 
			{
			   'page_id':page_id,
			   'action_id':action_id
			};
			request('/ajax/change_action',params).done(function(res)
			{
					$this.prop('disabled',false);
					if(res.code != 200){
						$this.prop('checked', $this.prop('checked') ? false : true).change();
					}
	
			});
	
		
 });
});


$(function() {
    $('#toggle-one').bootstrapToggle({
        on: 'On',
        off: 'Off'
    });

    $('#toggle-one').change(function(){
        var status = 0;
        if(repost_id == 0){
            if($(this).prop('checked') == true){
                $('#toggle-one').bootstrapToggle('off');
                notify('warning', 'Please setup page');
            }
            // 
            // $(this).prop('checked', $(this).prop('checked') ? false : true).change();
        }else{
            if($(this).prop('checked')){
                status = 1;
            }
            $.ajax({
                url: '/ajax/setStatus',
                type: 'post',
                data: {'repost_id': repost_id, 'status': status},
                success: function(){

                }
            })
        }
    });
});




$(document).on('keyup','.search-acc-insta',function(e){
    if(e.which == 13){
        
        if( !$.trim($(this).val())) {
            return false;   
        }
        
        $(this).parent().removeClass('open');
        
        $('.tab-pane.active .ajax-acc-insta').show();
        instaName = $.trim($(this).val());
        
        $('.tab-pane.active .ai-loader').show();
        $('.tab-pane.active .ai-content').hide();
        searchAccInsta(instaName)
            
    }
});

$('.save').click(function(){
    var delay0 = $('#delay0').val();
    var delay1 = $('#delay1').val();
    var delay_post = new Array(delay0, delay1);

    var execute0 = $('#execute0').val();
    var execute1 = $('#execute1').val();
    var execute_time = new Array(execute0, execute1);

    var post0 = $('#post0').val();
    var post1 = $('#post1').val();
    var post_day = new Array(post0, post1);

    var tag = $('#tag').val();
    // var remove_tag = $('#remove_tag').attr('checked') ? 1 : 0;
    // var remove_link = $('#remove_link').attr('checked') ? 1 : 0;
    var remove_tag = 0;
    var remove_link = 0;
    
    //console.log($('#remove_tag').is(":checked"));
    if($('#remove_tag').is(":checked"))
    {
        remove_tag = 1;
    }
    if($('#remove_link').is(":checked"))
    {
        remove_link = 1;
    }

    var setting = {
        'delay_post': delay_post,
        'execute_time': execute_time,
        'post_day': post_day,
        'hashtag': tag,
        'remove_tag': remove_tag,
        'remove_link': remove_link
    };
    //console.log(remove_tag);
  
    $.ajax({
        url:'/ajax/repost',
        type: 'post',
        data: {'repost_id': repost_id, 'page_id': page_id, 'setting': setting},
        // beforeSend: function(){
        //     _this.prop('disabled',true);
        // },
        success: function(res){
            repost_id = res;
            notify('success', 'success');
        }
        
    });
    //console.log(remove_link);
});
function searchAccInsta(keyword){
    $.ajax({
        url:'/search?type=fb_repost',
        type: 'post',
        data: {'keyword': keyword},
        // dataType:'json',
        success: function(res){
            var html1 = '<li><a>No data available!</a></li>';
            var html = res ? res : html1;

            $('.select-acc-insta').html(html);
            $('.ai-loader').hide();
            $('.ai-content').show();
        }   
    }); 
}


$(document).on('click','.select-acc-insta li',function(e) {
    e.stopPropagation();
    if(repost_id == 0)
    {
        alert('Hãy setup trước khi chọn source');
    }else{
        var pk = $(this).attr('data-key');
        
        if( $('.tab-pane.active .list-acc-insta li[data-key='+pk+']').length == 0){
            var html = $(this).find('.hidden').text();
            var data = JSON.parse(html);
            $.ajax({
                url: '/ajax/saveSourceRepost',
                type: 'post',
                data: {'repost_id': repost_id, 'data': data},
                success: function(res){
                    if(res === 1){
                        $(this).hidden();
                    }
                }
            });
        
            $(this).find('div:last').append('<i class="fa fa-times"></i>');
            $('.tab-pane.active .list-acc-insta ul').append($(this)[0].outerHTML);
        }

        $(this).remove();
    }
});
$(document).on('click','.select-acc-insta li a',function(e) {
    e.stopPropagation();
});

$(document).on('click','.list-acc-insta i',function(){
    var page_fbid = $(this).parents('li').attr('data-key');
    $.ajax({
        url: '/ajax/removeSourceRepost',
        type: 'post',
        data: {'repost_id': repost_id, 'page_fbid': page_fbid},
        success: function(res){
           
        }
    });
    $(this).parents('li').remove();
    
});

$(document).click(function(event) {
      
  if (!$(event.target).closest(".filter-acc-insta").length) {
    $('.ajax-acc-insta').hide();
  }
});

</script>