<?php 
foreach ($data as $value): ?>
<?php
	$save = !empty($row['aggregated_pin_data']['aggregated_stats']['saves']) ? $row['aggregated_pin_data']['aggregated_stats']['saves']: 0;
  
    $comment = !empty($row['comment_count']) ? $row['comment_count'] : 0;
    $this->load->helper('number');
 ?>
<tr>
	<td><img src="<?=$value['images']['736x']['url']?>" width="150" height="150"></td>
	<td><?=isset($value['description'])?$value['description']:'';?></td>
	<td>photo</td>
	<td>
		<div class="table-info table-info-pinterest">
		<i class="fas fa-thumbtack"></i> <?=number($save);?>
		<i class="fas fa-comments"></i> <?=number($comment);?>
		</div>
	</td>
	<td>
		<button class="btn  btn-default btn-save-post"><i class="far fa-save"></i> Save</button>
	</td>
</tr>
						 	 		
<?php endforeach; ?>