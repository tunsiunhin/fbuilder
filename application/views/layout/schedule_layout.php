<div class="container">
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home">Home </a></li>
	  <li><a data-toggle="tab" href="#finded">Result</a></li>
      <li class="pull-right"><input type="checkbox" class="toggle-event" data-toggle="toggle" value="2" data-onstyle="success" data-size="larger" <?=$schedule['status']==1?'checked':''?>></li>
      <li class="pull-right" style="margin-right: 10px;margin-top: 7px;">
        Next Run: <?php if($schedule['time_run']){echo  date('H:i d/m/Y',$schedule['time_run']);}else{echo 'N/A';}?>
      </li>
	</ul>
</div>
<?php
	if($schedule['setting'])
		$setting = json_decode($schedule['setting'],true);

 ?>
<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
   	<div class="container setting-table">
		<div class="row">
			<div class="col-md-12">

					<div class="panel panel-default ">
					<div class="panel-heading"><i class="fas fa-pencil-alt"></i> Setting</div>
					<div class="panel-body">
						<form action="javascript:;" class="form-setting">
							<div>
								<div class="fb-left">
									<span>Thời Gian Chạy</span>
								</div>
								<div class="fb-right">
								     <input class="form-control" name="time_run" type="number" min="0" max="23" value="<?=isset($setting['time_run'][0])?$setting['time_run'][0]:0?>"  placeholder="From">
				                       <span>to</span>
				                     <input class="form-control" name="time_run" type="number" min="0" max="23" value="<?=isset($setting['time_run'][1])?$setting['time_run'][1]:23?>"  placeholder="To">
			                    </div>
							</div>


							<div>
								<div class="fb-left">
									<span>Số Lượng Bài Post Trên Ngày</span>
								</div>
								<div class="fb-right">
								     <input class="form-control" name="post_per_day" type="number" min="0" value="<?=isset($setting['post_per_day'][0])?$setting['post_per_day'][0]:5?>"  placeholder="From">
				                       <span>to</span>
				                     <input class="form-control" name="post_per_day" type="number" min="0" value="<?=isset($setting['post_per_day'][1])?$setting['post_per_day'][1]:10?>"  placeholder="To">
			                    </div>
							</div>

							<div>
								<div class="fb-left">
									<span>Thời gian giữa mỗi bài post</span>
								</div>
								<div class="fb-right">
								     <input class="form-control" name="time_delay_post" type="number" min="0" value="<?=isset($setting['time_delay_post'][0])?$setting['time_delay_post'][0]:50?>"  placeholder="From">
				                       <span>to</span>
				                     <input class="form-control" name="time_delay_post" type="number" min="0" value="<?=isset($setting['time_delay_post'][1])?$setting['time_delay_post'][1]:100?>"  placeholder="To">
			                    </div>
							</div>

							<div>
								<div class="fb-left">
									<div class="checkbox">
                                      <label><input type="checkbox" class="add-hashtag" name="add_tag"  <?php if(isset($setting['add_tag']) && $setting['add_tag'] == 1){ echo 'checked';};?> value="1">Add Hashtag</label>
                                    </div>
								</div>
								<div class="fb-right">
								     <input class="form-control text-hashtag" name="hashtag" value="<?=isset($setting['hashtag'])?$setting['hashtag']:'';?>"   type="text" min="0" style="width: 483px" placeholder="Add hastag" <?=(!isset($setting['add_tag']) || $setting['add_tag'] != 1) ?' disabled="true"' : '';  ?>>    
			                    </div>
							</div>

							

							<div>
								<div class="fb-left">
									<button class="btn btn-success btn-sm btn-save-schedule"><i class="fa fa-save"></i> Save</button>
								</div>
							</div>

						</form>
					</div>
				</div>

			</div>
		
		</div>
	</div><!-- Container  -->

	<div class="container source-table">
		<div class="panel panel-default ">
			<div class="panel-heading"><i class="fab fa-cloudversify"></i> Source</div>
				<div class="panel-body source-body">

					<ul class="nav nav-tabs  nav-search">
					  <li class="active"><a data-toggle="tab" href="#facebook-search"><i class="fab fa-facebook-f"></i> Facebook </a></li>
					  <li><a data-toggle="tab" href="#instagram-search"><i class="fab fa-instagram"></i> Instagram </a></li>
					  <li><a data-toggle="tab" href="#pinterest-search"><i class="fab fa-pinterest-square"></i> Pinterest</a></li>
					</ul>
					<div class="tab-content">
						 <div id="facebook-search" class="tab-search tab-pane fade in active">
							<div >
								<div class="col-md-12 search-bar">
									<div id="custom-search-input">
						                <div class="input-group col-md-12">
						                    <input type="text" class="form-control input input-facebook" placeholder="Search.." />
						                    <span class="input-group-btn">
						                        <button class="btn btn-info btn-sm" type="button">
						                            <i class="fab fa-facebook-f"></i>
						                        </button>
						                    </span>

						                </div>
						            </div>
						           	 <button class="btn bnt-success btn-my-success">Search</button>
								</div>
							
								   <div class="search-sub search-sub-facebook">
						            	
						            </div>
							</div>
						          
								
							</div>
	
						 
						 <div id="instagram-search" class="tab-search  tab-pane fade">
						 			<div >
								<div class="col-md-12 search-bar">
									<div id="custom-search-input">
						                <div class="input-group col-md-12">
						                    <input type="text" class="form-control input input-instagram" placeholder="Search.." />
						                    <span class="input-group-btn">
						                        <button class="btn btn-info btn-sm" type="button">
						                            <i class="fab fa-instagram"></i>
						                        </button>
						                    </span>

						                </div>
						            </div>
						           	 <button class="btn bnt-success btn-my-success">Search</button>
								</div>
							
								   <div class="search-sub search-sub-instagram">
						            	
						            </div>
							</div>
						          
						 </div>

						 <div id="pinterest-search" class="tab-search tab-pane fade">
						 	<div >
								<div class="col-md-12 search-bar">
									<div id="custom-search-input">
						                <div class="input-group col-md-12">
						                    <input type="text" class="form-control input input-pinterest" placeholder="Search.." />
						                    <span class="input-group-btn">
						                        <button class="btn btn-info btn-sm" type="button">
						                            <i class="fab fa-pinterest-square"></i>
						                        </button>
						                    </span>

						                </div>
						            </div>
						           	 <button class="btn bnt-success btn-my-success">Search</button>
								</div>
							
								
							</div>
						 </div>
						 <div class="row">
						 	 <div class="col-md-12 result-ajax-content">
						 	 	<table class="table table-striped table-hover table-schedule">
						 	 		<thead>
						 	 			<tr>
						 	 				<td>Picture</td>
						 	 				<td>Content</td>
						 	 				<td>Type</td>
						 	 				<td style="width: 20%">Info</td>
						 	 				<td style="width: 15%">Action</td>
						 	 			</tr>
						 	 		</thead>
						 	 		<tbody>
						
						 	 		</tbody>
						 	 	</table>
						 	 	<button class="btn btn-default btn-load-more" style="margin-left:50%"><i class="fas fa-redo-alt"></i> Load More</button>
						 	 </div>
						 </div>
						
					</div>
			
				</div><!-- panel-body -->


		</div>
	</div><!-- Container  -->

  </div><!--  Home -->
  <div id="finded" class="tab-pane fade">
   	<div class="container">
        <div class="table-result">
               <table id="tab-result" class="table table-striped table-hover" >
                    <thead class="ádasd">
                        <tr>
                            <th class="tbr-p">Picture</th>
                            <th class="tbr-c">Content</th>
                            <th>Status</th>
                            <th>Time Posted</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                     	<?php foreach($result as $row){ ?>
                     	<tr data-id=<?=$row['sschedule_id']?>>
                            <td><a target="_blank" href="<?=isset($row['post_id'])?'https://fb.com/'.$row['post_id'] : 'https://fb.com'?>"><img src="<?=$row['picture']?>" width="50" height="50"></a></td>
                            <td><?=$row['content']?></td>
                            <td><?=$row['status']==1?'<span class="label label-success">Posted</span>':'<span class="label label-info">In Queue</span>'?></td>
                            <td><?=isset($row['time_posted'])?date('H:i d/m'):'N/A'?></td>
                            <td><?=$row['type']?></td>
                            <td><button class="btn btn-default btn-edit-post"><i class="fas fa-edit"></i> Edit</button> <button class="btn btn-default btn-remove-post"><i class="fas fa-trash"></i></button></td>
                        </tr> 
                     	<?php } ?>
                    </tbody>
                </table>
        </div>
    </div>
  </div>
  
</div><!-- Tabcontent -->
<div class="modal fade" id="edit_post" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fas fa-edit"></i> Edit Post</h4>
        </div>
        <div class="modal-body">
          <div class="form-account">
              <div class="form-group">
                 <label>Edit</label>
                 <textarea type="text" class="form-control value-edit-text" placeholder="" rows="10"> </textarea>
              </div>
     		 <div class="form-group ">
     		 	<button type="button" class="btn btn-success btn-save-edit"><i class="fas fa-save"></i> Lưu</button>
          		<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
     		 </div>
     
          </div>
        </div>
     
      </div>
      
    </div>
  </div> 
<script>
	$(document).ready(function(){
		var cursor,url,data_id,content,col_tr;
		var page_id = <?=$this->input->get('page_id');?>;
		$('.nav-search li').click(function(){
			clear_box();
		});
		$('.input-facebook').keyup(function(e){
			if(e.keyCode ==13 && $('.input-facebook').val() != '')
			{
				var q = $('.input-facebook').val().replace(' ','+');
				
				$('.search-sub-facebook').html('<div class="ai-loader text-center" style="margin-top:20px"><i aria-hidden="true" hidden="" class="fa fa-spinner fa-spin"></i></div>').show();
				search_rq('/search?type=facebook&q='+q).done(function(res){
				if(res.code == 200)
				{
					$('.search-sub-facebook').html(res.html).show();
	
				}
			 });
			}
	
	
		});
		$('.input-instagram').keyup(function(e){

			if(e.keyCode ==13 && $('.input-instagram').val() != '')
			{
				var q = $('.input-instagram').val().replace(' ','+');
				$('.search-sub-instagram').html('<div class="ai-loader text-center" style="margin-top:20px"><i aria-hidden="true" hidden="" class="fa fa-spinner fa-spin"></i></div>').show();
				search_rq('/search?type=instagram&q='+q).done(function(res){
				if(res.code == 200)
				{
					$('.search-sub-instagram').html(res.html).show();
				}
				});
			}
	
	
		});

		$(document).on('click','.search-sub-facebook ul li ',function(e){
			$('.search-sub').hide().html(' ');
			page_fbid = $(this).attr('data-id');
			url = '/search?type=facebook_feed&page_id='+page_fbid;
			search_rq(url).done(function(res){
				if(res.code == 200)
				{
					$('.result-ajax-content').show();
					$('.table-schedule tbody').html(res.html);
					if(res.cursor)
					{
						cursor = res.cursor;
					}
	
				}
				});
		});
		$(document).on('click','.search-sub-instagram ul li ',function(e){
			$('.search-sub').hide().html(' ');
			var username = $(this).attr('username');
			var pk		 = $(this).attr('data-id');
			url = '/search?type=instagram_feed&username='+username+'&pk='+pk;
			search_rq(url).done(function(res){
				if(res.code == 200)
				{
					$('.result-ajax-content').show();
					$('.table-schedule tbody').html(res.html);
					if(res.cursor)
					{
						cursor = res.cursor;
					}
	
				}
				});
		});
		$('.input-pinterest').keyup(function(e){
			if(e.keyCode ==13 && $('.input-pinterest').val() != '')
			{
				var q = $('.input-pinterest').val();
				search_rq('/search?type=pinterest&q='+q).done(function(res){
			
				if(res.code == 200)
				{
					$('.result-ajax-content').show();
					$('.table-schedule tbody').html(res.html);
				}
				});
			}
		});
		$(document).on('click','.btn-save-post',function(e){
				var tr = $(this).parents('tr');
				var text = tr.find('td').eq(1).text();
				var picture = tr.find('td:first img').attr('src');
<<<<<<< HEAD
				var source = tr.find('td:first').attr('data-source');
				var type = tr.find('td').eq(2).text();
				$.ajax({
					url:'/ajax/postSchedule',
					method:'post',
					data:{'text':text,'picture':picture,'page_id':page_id,'type':type,'source':source},
					beforceSend:function()
					{},
=======
				var source = tr.attr('data-source');
				$.ajax({
					url:'/ajax/postSchedule',
					method:'post',
					data:{'text':text,'picture':picture,'page_id':page_id,'source':source},
					beforceSend:function(){},
>>>>>>> 401d9b8779d710933aed08e7025b617a4104c8b2
					success:function(res)
					{
						if(res.code == 200)
						{
							tr.addClass('success');
							notify('success',res.message);
						}else if(res.code == 400)
						{
							notify('danger',res.message);
						}
					}
				})

		});
		$('.btn-remove-post').click(function(){
			var tr = $(this).parents('tr');
			var id = tr.attr('data-id');
			search_rq('/ajax/removePost','post',{'id':id,'page_id':page_id}).done(function(res)
			{
				if(res.code == 200)
				{		
					tr.hide();
					notify('success',res.message);
				}else if(res.code == 400)
				{
					notify('danger',res.message);
				}
			});
		});
		$('.btn-edit-post').click(function(){
			data_id = '';
			content = '';
			col_tr = $(this).parents('tr');
			data_id = col_tr.attr('data-id');
			content = col_tr.find('td').eq(1).text();
			$('.value-edit-text').text(content);
			$('#edit_post').modal('show');
			
		});
		$('.btn-save-edit').click(function(){
			content = $('.value-edit-text').val();
			search_rq('/ajax/editPost','post',{'id':data_id,'content':content,'page_id':page_id}).done(function(res)
			{
				if(res.code == 200)
				{		
					col_tr.find('td').eq(1).text(content);
					notify('success',res.message);
				}else if(res.code == 400)
				{
					notify('danger',res.message);
				}
			});
		});
		$('.btn-load-more').click(function(){
	
			if(!cursor)
			{
				notify('warning','Đã hết');
				return;
			}
			_this = $(this);
			_this.find('i').addClass('fa-spin');

			search_rq(url+'&cursor='+cursor).done(function(res){
				$('.table-schedule tbody').append(res.html);
				_this.find('i').removeClass('fa-spin');
				if(res.cursor)
						cursor = res.cursor;
	
			});

		});
		$('.toggle').click(function(){
		
			var $this  = $(this).find('input');
			if($this.prop('disabled'))
			{
				return false;
			}
			var action_id = $this.val();
			var status    = ($this.is(':checked') ) ? 1 : 0;
			var params = 
			{
			   'page_id':page_id,
			   'action_id':action_id
			};
			request('/ajax/change_action',params).done(function(res)
			{
					$this.prop('disabled',false);
					if(res.code != 200){
						$this.prop('checked', $this.prop('checked') ? false : true).change();
					}
	
			});
	
		
		});
		function search_rq(url,method="GET",params='')
		{
			var rq = $.ajax({
				'url':url,
				'data':params,
				'method':method,
				dataType:'json'
			});
			return rq;
		}
		function clear_box($submenu = true,$table =true)
		{
			cursor = '';
			url = '';
			if($table)
			{
				$('.result-ajax-content').hide();
				$('.table-schedule tbody').html(' ');
				
			}
			if($submenu)
				$('.search-sub').hide().html(' ');
				
		}
	});
</script>