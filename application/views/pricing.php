<style type="text/css">
	.plan{
		margin-top: 100px;
	}
	.plan .col{
		position: relative;
		border: 1px solid #ddd;
		text-align: center;
		border-radius: 3px;
	}
	/*.plan .row{
		width: 500px;
		margin: auto;
	}*/
	.plan .name{
		font-weight: 600;
		font-size: 19px;
		padding-top: 15px;
	}
	.plan .price{
		padding: 15px 0;
		color: #999;
	}
	.plan .price strong{
		font-size: 32px;
		color: #1e88e5;
	}
	.plan .info{
		padding: 0 15px;
		height: 200px;
	}
	.plan .info p{
		border-bottom: 1px dotted #eee;
		padding-bottom: 12px;
		margin-bottom: 12px;
	}
	.plan .action{
		padding: 15px 0;
	}
	.method>div{
		border:1px solid #ddd;
		margin-bottom:15px;
		border-radius:3px;	
	}
	.method .heading{
		padding:10px;
		cursor:pointer;	
	}
	.method .heading span{
		display:inline-block;
		width:16px;
		height:16px;
		border:1px solid #ccc;
		border-radius:50%;
		float:left;
		margin-right:7px;
		margin-top:2px;
		position:relative;
	}
	.method .heading span:after{
		content:'';
		position:absolute;
		width:6px;
		height:6px;
		border-radius:50%;
		background:#999;
		top:4px;
		left:4px;
		opacity:0;	
	}
	
	.method .content{
		display:none;
			
	}
	.method>div.active{
		background:#f6f7f9;
		border-color:#cdd9f0;	
	}
	.method>div.active .content{
		display:block;	
	}
	.method>div.active .heading span:after{
		opacity:1;
	}
	.method .paypal .content{
		text-align:center;
		padding-bottom:15px;
		
	}
	.method .paypal .content input[type="image"]{
		outline:none
	}
	.method .payoneer .content{
		padding:0 15px 15px 15px;	
	}
	.method .bank .content{
		padding:0 15px;	
	}
	.method .bank .content>div{
		margin-bottom:10px;	
	}		
	.method .bank p{
		margin:0;	
	}
</style>

<div class="container">
	<div class="text-center">
		<h2>Make Payment</h2>
		<p>Choose a plan below and select a payment method</p>
	</div>
	<div class="plan">


		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-3">
				<div class="col">
					<div class="name">Trial</div>
					<div class="price">
						<strong>$0</strong>
					</div>
					<div class="info">
						<p>dasds</p>
						<p>sdgg</p>
					</div>
					<div class="action">
						<button class="btn btn-success" type="button" disabled="disabled">Buy now</button>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="col">
					<div class="name">Basic</div>
					<div class="price">
						<strong>$100/</strong>
						Month
					</div>
					<div class="info">
						<p>dasds</p>
						<p>sdgg</p>
					</div>
					<div class="action">
						<button class="btn btn-success buy-now" value="E9MHPEJW3A9T2" type="button">Buy Now</button>
					</div>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</div>

<div id="methodModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Select a Payment Method</h4>
          </div>
          <div class="modal-body method">
            <div class="paypal active">
            	<div class="heading">
                	<span></span>
                    <label>Paypal</label>
                </div>
                <div class="content">
                	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="custom" value="thaint32@wru.vn">
                        <input type="hidden" class="paypal_plan" name="hosted_button_id" value="">
                        <input type="image" src="assets/img/checkout.png" width="200" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                   </form>
                </div>
            </div>
            <div class="bank">
            	<div class="heading">
                	<span></span>
                    <label>Bank</label>
                </div>
                <div class="content">
                    <div>
                        <p>* Gói Basic: <b>380.000</b> vnđ</p>
                        <p style="margin-top:5px">* Gói Standard: <b>600.000</b> vnđ </p>
                        <p style="margin-top:5px">* Gói Unlimited: <b>820.000</b> vnđ </p>
                    </div>
                     <div>
                        <p><label>1. </label> <span style="margin-left:5px;">Ngân Hàng <b>ACB</b></span></p>
                        <p style="padding-left:25px"><span>Số tài khoản 666698</span>, <span>Vũ Duy Nguyên - Nguyễn Khánh Toàn, Hà Nội</span></p>
                     </div>
                     <div>
                        <p><label>2. </label> <span style="margin-left:5px;">Ngân Hàng <b>VIETCOMBANK</b></span></p>
                        <p style="padding-left:25px"><span>Số tài khoản 0491000141134</span>, <span>Vũ Duy Nguyên</span></p>
                     </div>
                    <!-- <div>
                        <p><label>3. </label> <span style="margin-left:5px;">Ngân Hàng <b>TECHCOMBANK</b></span></p>
                        <p style="padding-left:25px"><span>Số tài khoản 19023357718024</span>, <span>Vũ Duy Nguyên - Nguyễn Khánh Toàn, Hà Nội</span></p>
                     </div>-->
                     <div>
                       => Nội dung chuyển khoản là thanh toán Tool KingTarget + Email và gửi hóa đơn về  <a href="https://www.facebook.com/messages/t/nguyenvuteam" target="_blank">Nguyen Vu Team</a>
                     </div>
                </div>
            </div>
            <div class="payoneer">
            	<div class="heading">
                	<span></span>
                    <label>Payoneer</label>
                </div>
                <div class="content">
                	Please send to Payoneer <strong>vuduynguyen8x@gmail.com</strong> and contact <a href="https://www.facebook.com/messages/t/nguyenvuteam" target="_blank">Nguyen Vu Team</a> to active account
                </div>
            </div>
            
            <div class="paypal">
            	<div class="heading">
                	<span></span>
                    <label>LightCoin</label>
                </div>
                <div class="content">
                	This payment method is not available!
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    
    </div>
</div>

<script>
	$('.method .heading').bind('click',function(){
			$('.method>div.active').removeClass('active');
			$(this).parent().addClass('active');	
		});

	$('.buy-now').bind('click',function(e) {
        hosted_id = $(this).val();
		if(!hosted_id)
			return false;
		$('.paypal_plan').val(hosted_id);
		$('#methodModal').modal('toggle');
		
    });
</script>  
