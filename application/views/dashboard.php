<div class="container panel-notify">
  <?php 
    if(isset($panel_notify))
    {

        foreach($panel_notify as $panel)
        {
          echo $panel;
        }
    }       
  ?>
</div>
    <div class="container">
    <div class="row">
    	<div class="col-md-6">
        <div class="input-search">
    			<div class="dropdown">
    				<i class="fa fa-search"></i>
    				<input class="form-control text-search dropdown-toggle" data-toggle="dropdown" placeholder="Search Page" type="search" aria-expanded="true">

    				<div class="dropdown-menu">
    					<div style="padding:5px 10px">
    						Press Enter to search
    					</div>
    				</div>
    			</div>
    		</div>
       </div>
       <div class="col-md-6 add-accout">
         <button class="btn btn-warning btn-show-token"><i class="fa fa-plus"></i> Thêm Tài Khoản</button>
       </div>
      </div>
    </div>
    
    <div class="container">
    	<table class="table table-hover table-striped table-pages">
        	<thead>
            	<tr>
                	<td class="tb-picture">Picture</td>
                    <td class="tb-name">Name</td>
                    <td>Repost</td>
                    <td>Schedule</td>
                    <td class="tb-action">Action</td>
                </tr>
            </thead>
            <tbody>
              <?php 
              $this->load->helper('number');
              foreach ($pages as $page){?>
                  <tr data-id="<?=$page['page_id']?>">
                    <td>
                        <img class="img-responsive img-circle" src="<?=$page['picture']?>" width="50" height="50" >
                      </td>
                      <td>
                        <div><a href="https://fb.com/<?=$page['page_fbid']?>" target="_blank"><strong><?=$page['name']?></strong></a></div>
                          <div>
                            <span>
                                <i class="far fa-thumbs-up"></i> <?=number($page['fan_count'])?>
                            </span>
                          </div>
                      </td>
                      <td>
                        <div class="text-result">
                            <span><?=($page['repost_result']) ? number($page['repost_result']) : 0 ?></span>   
                        </div>
                        <div>
                           <input type="checkbox" class="toggle-event" data-toggle="toggle" value="1" data-onstyle="success" data-size="mini" <?=($page['repost_status'] && $page['repost_status'] == 1) ? 'checked' : '' ?>>
                        </div>
              
                      </td>

                      <td>
                        <div class="text-result">
                            <span><?=($page['schedule_result']) ? number($page['schedule_result']) : 0 ?></span>   
                        </div>
                        <div>
                           <input type="checkbox" class="toggle-event" data-toggle="toggle" value="2" data-onstyle="success" data-size="mini" <?=($page['schedule_status'] && $page['schedule_status'] == 1) ? 'checked' : '' ?>>
                        </div>
                      </td>

                     


                      <td class="row-action">
                        <a href="app/repost?page_id=<?=$page['page_id']?>" class="btn btn-default btn-action"><i class="fa fa-redo"></i> Repost</a>
                        <a href="app/schedule?page_id=<?=$page['page_id']?>" class="btn btn-default btn-action"><i class="fas fa-calendar-alt"></i> Schedule</a>
                        <div class="dropdown btn-action">
                          <button class="btn btn-default btn-action  dropdown-toggle"  data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button>
                          <ul class="dropdown-menu dropdown-menu-right">
                           <li><a href="#">Insight</a></li>
                          </ul>
                        </div>
                      </td>
                  </tr>    
              <?php } ?>
              
            </tbody>
        </table>
    </div>
 <script>
 	$(document).ready(function()
	{
		$('#tab-result').DataTable();
		$('.btn-show-token').click(function() {
			$('#token_modal').modal('show');
		});
	
		$('.btn-save-token').click(function(){
			var token = $('.account-token').val();
			_this = $(this);
			if (!token)
			{
				notify('warning', 'Please input access token');
				return;
			}
	
			_this.prop('disabled',true);
			request('/api/token',{'token':token}).done(function(res){
				_this.prop('disabled',false);
				if(res.code == 200)
				{
					setTimeout(function(){
						location.href="/";
					},2000);
				}
			});
		});
		
		// Toggle action
		$('.toggle').click(function(){
			var page_id;
			var $this  = $(this).find('input');
			if($this.prop('disabled'))
			{
				return false;
			}
			page_id    = $this.parents('tr').attr('data-id');
			var action_id = $this.val();
			var status    = ( $this.is(':checked') ) ? 1 : 0;
			var params = 
			{
			   'page_id':page_id,
			   'action_id':action_id
			};
			request('/ajax/change_action',params).done(function(res)
			{
					$this.prop('disabled',false);
					if(res.code != 200){
						$this.prop('checked', $this.prop('checked') ? false : true).change();
					}
	
			});
	
		
		});
		$('.text-search').keyup(function(e){
			var textSearch = $(this).val();
			if(e.keyCode == 13)
			{
				if(!textSearch)
					return;
				location.href="?search="+textSearch;
			}
		});
		
		// request
	});

 </script>
    
