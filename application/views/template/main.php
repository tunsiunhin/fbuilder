<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fanpagebuilder</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css">
  <link rel="stylesheet" href=" https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/main.css?v=<?=time()?>">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
 
  <script src="/assets/js/main.js"></script>
</head>
<body>
<nav class="navbar navbar-custom">
       <div class="container">
          <div class="navbar-header">
             <a style="padding: 10px 0; margin: 0" class="navbar-brand" href="/app">
               <img src="<?= base_url()?>assets/image/flogo.png"  width="160" height="30">
             </a>
          </div>
          <ul class="nav navbar-nav">
             <li ><a href="/app">Dashboard</a></li>
             <li ><a href="/app/repost">Repost</a></li>
             <li ><a href="/app/schedule">Schedule</a></li>
             <li ><a href="/app/guilde">Guild</a></li>
             <li ><a href="/app/pricing">Pricing</a></li>
             <li><a href="https://goo.gl/scbq6M" target="_blank">Extension</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><i class="fas fa-life-ring"></i> Support</a></li>
            <li class="dropdown btn-account"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">Account   <i class="fa fa-caret-down"></i> </a>
                 <ul class="dropdown-menu">
                            <li><a href="#"><i class="fas fa-user"></i> Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                  </ul>
            
           
            </li>

          </ul>
        </div>
</nav>
  <div class="container schedule-breadcrumb">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li><a href="/app"><i class="fa fa-home"></i> App</a></li>
              <li class="breadcrumb-item active" aria-current="page" style="text-transform:capitalize ;">
                <?php 
                    $uri = $this->uri->slash_segment(2);
                    if($uri == '/')
                      echo 'Overview';
                    else
                      echo str_replace("/"," ",$uri);
                 ?>
              </li>
            </ol>
          </nav>
        </div>

        <?php $this->load->view($view); ?>
</body>
</html>