<!DOCTYPE html>
<html lang="en">
<head>
  <title>Fbuilder - Fbuilder.pro</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link href="/assets/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/bootstrap-select.css">
  <link rel="stylesheet" href="/assets/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/animate.css">
  <link rel="stylesheet" href="/assets/css/main.css?v=<?=time()?>">

  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/bootstrap-toggle.min.js"></script>
  <script src="/assets/js/bootstrap-select.min.js"></script>

  <script src="/plugins/DataTables/jquery.dataTables.min.js"></script>
  <script src="/plugins/DataTables/dataTables.bootstrap.min.js"></script>
  <script src="/assets/js/bootstrap-notify.min.js"></script>
 
  <script src="/assets/js/main.js?v=<?=time();;?>"></script>
</head>
<body>
<?php $uri = $this->uri->slash_segment(2);?>
<nav class="navbar navbar-custom">
       <div class="container">
          <div class="navbar-header">
             <a style="padding: 10px 0; margin: 0" class="navbar-brand" href="/app">
               <img src="<?= base_url()?>assets/image/flogo.png"  width="160" height="30">
             </a>
          </div>
          <ul class="nav navbar-nav">
             <li ><a href="/app" class="<?=$uri=='/'?'active':''?>">Dashboard</a></li>
             <li ><a href="/app/repost" class="<?=$uri=='repost/'?'active':''?>">Repost</a></li>
             <li ><a href="/app/schedule" class="<?=$uri=='schedule/'?'active':''?>">Schedule</a></li>
             <li ><a href="/app/guilde" class="<?=$uri=='guilde/'?'active':''?>">Guild</a></li>
             <li ><a href="/app/pricing" class="<?=$uri=='pricing/'?'active':''?>">Pricing</a></li>
             <li><a href="https://goo.gl/scbq6M" target="_blank">Extension</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a target="_blank" href="https://messenger.com/t/nguyenvuteam"><i class="fas fa-life-ring"></i> Support</a></li>
            <li class="dropdown btn-account"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">Account   <i class="fa fa-caret-down"></i> </a>
                 <ul class="dropdown-menu">
                            <li><a href="#"><i class="fas fa-user"></i> Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                  </ul>
            
           
            </li>

          </ul>
        </div>
</nav>
  <div class="container schedule-breadcrumb">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li><a href="/app"><i class="fa fa-home"></i> App</a></li>
              <li class="breadcrumb-item active" aria-current="page" style="text-transform:capitalize ;">
                <?php 
                    if($uri == '/')
                      echo 'Overview';
                    else
                      echo str_replace("/"," ",$uri);
                 ?>
              </li>
            </ol>
          </nav>
        </div>