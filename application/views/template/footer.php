<!-- Modal footer -->
<div class="modal fade" id="token_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Thêm Tài Khoản</h4>
        </div>
        <div class="modal-body">
          <div class="form-account">
              <div class="form-group">
                 <label>Access Token</label>
                 <input type="text" class="form-control account-token" placeholder="Access Token">
              </div>
     		 <div class="form-group token_button">
     		 	<button type="button" class="btn btn-success btn-save-token"><i class="fas fa-save"></i> Lưu</button>
          		<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
     		 </div>
     
          </div>
        </div>
     
      </div>
      
    </div>
  </div> 
</body>
</html>