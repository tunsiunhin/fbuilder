<style type="text/css">
	.asc-left{
		width: 25%;
		vertical-align: top;
		display: inline-block;
	}
	.asc-left i{
		color: #999;
		margin-left: 5px;
		font-size: 12px;
		padding: 2px;
	}
	.asc-right{
		width: 50%;
		margin-bottom: 20px;
		display: inline-block;
	}
	.asc-right input{
		width: calc(50% - 19px);
		display: inline-block;
		padding: 5px 12px;
		height: 30px;
	}
	.asc-right span{
		display: inline-block;
		width: 30px;
		text-align: center;
		color: #999;
	}
	.info{
		border-bottom: 1px solid #eee;
		margin-bottom: 20px;
		overflow: hidden;
	}
	.info>.left{
		float: left;
		text-align: center;
		height: 80px;
		line-height: 80px;
	}
	.left-img{
		float: left;
	}

</style>

<div class="container">
 	<ul class="breadcrumb">
		<li><a href="#"><i class="fa fa-home"></i> App</a></li>
		<li class="active">Upload</li>
	</ul>
</div>

<div class="container">
	<div class="info">
		<div class="left">
			<div class="left-img">
				<img src="">
			</div>
			<div class="left-text">
				<span>Upload</span>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<i class="fas fa-pencil-alt"></i>
			Setting
		</div>
		<div class="panel-body">
			<form>
				<div>
					<div class="asc-left">
						<span>Select page</span>
					</div>
					<div class="asc-right">
						<select class="form-control">
							<option value="">aaa</option>
						</select>
					</div>
				</div>
				<div>
					<div class="asc-left">
						<span>Execute time</span>
						<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Khoảng thời gian đăng bài post trong ngày, đơn vị là giờ"></i>
					</div>

					<div class="asc-right">
						<input class="form-control" type="number" min="0" value="0" name="rpd" placeholder="From">
						<span>to</span>
						<input class="form-control" type="number" min="0" value="23" name="rpd" placeholder="To">
					</div>
				</div>

				<div>
					<div class="asc-left">
						<span>Time delay each post</span>
						<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Khoảng thời gian giữa mỗi post, đơn vị là phút"></i>
					</div>

					<div class="asc-right">
						<input class="form-control" type="number" min="0" value="30" name="rpd" placeholder="From">
						<span>to</span>
						<input class="form-control" type="number" min="0" value="45" name="rpd" placeholder="To">
					</div>
				</div>

				<div>
					<div class="asc-left">
						<span>Maximum post per day</span>
						<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Số post tối đa 1 ngày"></i>
					</div>

					<div class="asc-right">
						<input class="form-control" type="number" min="0" value="3" name="rpd" placeholder="From">
						<span>to</span>
						<input class="form-control" type="number" min="0" value="5" name="rpd" placeholder="To">
					</div>
				</div>

				<div class="asc-left"></div>
				<div class="asc-right">
					<button type="submit" class="btn btn-primary" style="float: right;">Apply Setup</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>