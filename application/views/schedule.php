<div class="container">
	<div class="content-header clearfix">
		<div class="content-left">
			<i class="far fa-calendar-alt"></i>  Schedule  | <?=$schedule ? $schedule['name'] : 'N/A';?>
		</div>
		<div class="content-right">
			<select class="selectpicker show-tick show-menu-arrow select-page" data-live-search="true" title="Select Page" data-size="10">
			 	<?php foreach ($allpage as $page): ?>
			 		 <option value="<?=$page['page_id']?>" ><?=$page['name']?></option>
			 	<?php endforeach ?>
			</select>

		</div>
	</div>
</div>
<?=$layout;?>
<script >
	var page_id = <?=isset($_GET['page_id'])?$_GET['page_id']:0;?>;
	$(document).ready(function(){
   	 $('#tab-result').DataTable();
	$('.select-page').change(function(){
		var page_id = $(this).val();
		location.href = "?page_id="+page_id;
	});
	$('.add-hashtag').click(function(){
		if($('.add-hashtag').is(":checked"))
		{
			$('.text-hashtag').prop('disabled',false);
		}else
		{
			$('.text-hashtag').prop('disabled',true);
		}
	});
	$('.btn-save-schedule').click(function(){
		$this = $(this);
		var params = {
			'page_id':page_id,
			'setting':{},	
		};
		var formData = $('.form-setting').serializeArray();
		$.each(formData,function(k,row){
			if(row['name'] in params['setting']) {
				params['setting'][row['name']].push(row['value']);	
			}	
			else {
				params['setting'][row['name']] = [row['value']];	
			}
		});
		$this.prop('disabled',true);
		request('/ajax/save_schedule',params).done(function(res){
			$this.prop('disabled',false);
		});
	});
    });
</script>