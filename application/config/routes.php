<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "home/index";
$route['app']				 = "home/index";
$route['app/repost']		 = "home/repost";
$route['app/schedule']	 	 = "home/schedule";
$route['app/upload']		 = "home/upload";
$route['app/guilde']		 = "home/guilde";
$route['app/pricing']		 = "home/pricing";
$route['search']			 = "search/index";
$route['404_override'] = '';
$route['cronjob/schedule/(:num)'] = 'cronjob/schedule/$1';
$route['cronjob/repost/(:num)'] = 'cronjob/repost/$1';
$route['execute/post_video/(:num)'] = 'execute/post_video/$1';
$route['authentication']   = 'auth/index';
