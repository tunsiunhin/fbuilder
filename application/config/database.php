<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$active_group = 'localhost';
$active_record = TRUE;

//$db['default']['hostname'] = '144.202.88.123';
//$db['default']['username'] = 'midland';
//$db['default']['password'] = 'c4lXDo9vpxk6zOhi';
//$db['default']['database'] = 'midland';
//$db['default']['dbdriver'] = 'mysqli';
//$db['default']['dbprefix'] = '';
//$db['default']['pconnect'] = TRUE;
//$db['default']['db_debug'] = TRUE;
//$db['default']['cache_on'] = FALSE;
//$db['default']['cachedir'] = '';
//$db['default']['char_set'] = 'utf8';
//$db['default']['dbcollat'] = 'utf8_general_ci';
//$db['default']['swap_pre'] = '';
//$db['default']['autoinit'] = TRUE;
//$db['default']['stricton'] = FALSE;


$db['localhost']['hostname'] = 'localhost';
$db['localhost']['username'] = 'root';
$db['localhost']['password'] = '';
$db['localhost']['database'] = 'fbuilder';
$db['localhost']['dbdriver'] = 'mysqli';
$db['localhost']['dbprefix'] = '';
$db['localhost']['pconnect'] = TRUE;
$db['localhost']['db_debug'] = TRUE;
$db['localhost']['cache_on'] = FALSE;
$db['localhost']['cachedir'] = '';
$db['localhost']['char_set'] = 'utf8mb4';
$db['localhost']['dbcollat'] = 'utf8mb4_unicode_ci';
$db['localhost']['swap_pre'] = '';
$db['localhost']['autoinit'] = TRUE;
$db['localhost']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */